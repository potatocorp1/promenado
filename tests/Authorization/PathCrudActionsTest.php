<?php

namespace Tests\Authorization;

use App\Models\Path;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PathCrudActionsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Path creation
     */

    public function test_guest_can_not_see_path_creation_form(): void
    {
        $response = $this->get('/path/create');
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_basic_user_can_not_see_path_creation_form(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 0]);
        $response = $this->actingAs($user)->get('/path/create');
        $response->assertForbidden();
        $response->assertSee('pas les autorisations nécessaires');
    }

    public function test_admin_user_can_see_path_creation_form(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 1]);
        $response = $this->actingAs($user)->get('/path/create');
        $response->assertStatus(200);
    }

    /**
     * Path deletion
     */

    public function test_guest_can_not_delete_path(): void
    {
        $path = Path::factory()->create();
        $response = $this->delete('/path/' . $path->id);
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_basic_user_can_not_delete_path(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 0]);
        $path = Path::factory()->create();
        $response = $this->actingAs($user)->delete('/path/' . $path->id);
        $response->assertForbidden();
        $response->assertSee('pas les autorisations nécessaires');
    }

    /**
     * Path update
     */

    public function test_guest_can_not_see_path_update_form(): void
    {
        $path = Path::factory()->create();
        $response = $this->get('/path/'.$path->id.'/edit');
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_basic_user_can_not_see_path_update_form(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 0]);
        $path = Path::factory()->create();
        $response = $this->actingAs($user)->get('/path/'.$path->id.'/edit');
        $response->assertForbidden();
        $response->assertSee('pas les autorisations nécessaires');
    }

    public function test_path_update_form_can_be_rendered_for_admin(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 1]);
        $path = Path::factory()->create([
            'name' => 'foo',
        ]);
        $response = $this->actingAs($user)->get('/path/'.$path->id.'/edit');
        $response->assertStatus(200);
        $response->assertSee('foo');
    }

    /**
     * Path start process
     */

    public function test_guest_can_not_start_a_path(): void
    {
        $path = Path::factory()->create();
        $response = $this->post(route('path.start', $path));
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }
}
