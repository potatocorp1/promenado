<?php

namespace Tests\Authorization;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AboutPageTest extends TestCase
{
    use RefreshDatabase;

    public function test_everybody_can_view_about_page(): void
    {
        $response = $this->get('/about');
        $response->assertStatus(200);
    }
}
