<?php

namespace Tests\Authorization;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StatPageTest extends TestCase
{
    use RefreshDatabase;

    public function test_admin_has_stat_page_link(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 1]);
        $response = $this->actingAs($user)->get('/');
        $response->assertSee('Statistiques');
    }

    public function test_basic_user_does_not_have_stat_page_link(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 0]);
        $response = $this->actingAs($user)->get('/');
        $response->assertDontSee('Statistiques');
    }
}
