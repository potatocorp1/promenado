<?php

namespace Tests\Authorization;

use App\Models\Path;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PathAdminPageTest extends TestCase
{
    use RefreshDatabase;

    public function test_guest_can_not_see_path_admin_show(): void
    {
        $path = Path::factory()->create();
        $response = $this->get('/admin/path/' . $path->id);
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_basic_user_can_not_see_path_admin_show(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 0]);
        $path = Path::factory()->create();
        $response = $this->actingAs($user)->get('/admin/path/' . $path->id);
        $response->assertForbidden();
        $response->assertSee('pas les autorisations nécessaires');
    }

    public function test_admin_user_can_see_path_admin_show(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 1]);
        $path = Path::factory()->create();
        $response = $this->actingAs($user)->get(route('path.show', $path));

        $response->assertViewHas('path', $path);
        $response->assertStatus(200);
    }
}
