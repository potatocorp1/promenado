<?php

namespace Tests\Authorization;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class WelcomePageTest extends TestCase
{
    use RefreshDatabase;

    public function test_everybody_can_view_welcome_page(): void
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
}
