<?php

namespace Tests\Authorization;

use App\Models\Path;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PathPublicPageTest extends TestCase
{
    use RefreshDatabase;

    public function test_everybody_can_see_a_path_public_view(): void
    {
        $path = Path::factory()->create();
        $response = $this->get(route('path.show', $path));

        $response->assertViewHas('path', $path);
        $response->assertStatus(200);
    }
}
