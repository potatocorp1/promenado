<?php

namespace Tests\Authorization;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StepCrudActionsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Step creation process
     */

    public function test_guest_can_not_see_step_creation_form(): void
    {
        $path = Path::factory()->create();
        $response = $this->get('/path/'.$path->id.'/step/create');
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_basic_user_can_not_see_step_creation_form(): void
    {
        $user = User::factory()->create(['is_admin' => 0]);
        $path = Path::factory()->create();
        $response = $this->actingAs($user)->get('/path/'.$path->id.'/step/create');
        $response->assertForbidden();
        $response->assertSee('pas les autorisations nécessaires');
    }

    public function test_admin_user_can_see_step_creation_form(): void
    {
        $user = User::factory()->create(['is_admin' => 1]);
        $path = Path::factory()->create();
        $response = $this->actingAs($user)->get('/path/'.$path->id.'/step/create');
        $response->assertStatus(200);
        $response->assertSee('Nouvelle étape');
    }

    /**
     * Step deletion
     */

    public function test_guest_can_not_delete_step(): void
    {
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $response = $this->delete('/step/' . $step->id);
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_basic_user_can_not_delete_step(): void
    {
        $user = User::factory()->create(['is_admin' => 0]);
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $response = $this->actingAs($user)->delete('/step/' . $step->id);
        $response->assertForbidden();
        $response->assertSee('pas les autorisations nécessaires');
    }

    /**
     * Step update process
     */

    public function test_guest_can_not_see_step_update_form(): void
    {
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $response = $this->get('/step/'.$step->id.'/edit');
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_basic_user_can_not_see_step_update_form(): void
    {
        $user = User::factory()->create(['is_admin' => 0]);
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/edit');
        $response->assertForbidden();
        $response->assertSee('pas les autorisations nécessaires');
    }

    public function test_admin_user_can_see_step_update_form(): void
    {
        $user = User::factory()->create(['is_admin' => 1]);
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'name' => 'foo',
            'answer_options' => ['option1', 'option2', 'option3']
        ]);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/edit');
        $response->assertStatus(200);
        $response->assertSee('foo');
        $response->assertSee('option1');
    }

    public function test_guest_can_not_update_step(): void
    {
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $response = $this->put('/step/'.$step->id, []);
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_basic_user_can_not_update_step(): void
    {
        $user = User::factory()->create(['is_admin' => 0]);
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $response = $this->actingAs($user)->put('/step/'.$step->id, []);
        $response->assertForbidden();
        $response->assertSee('pas les autorisations nécessaires');
    }

    /**
     * Step completion
     */

    public function test_guest_can_not_complete_a_step(): void
    {
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $response = $this->post('/step/'.$step->id.'/complete');
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }
}
