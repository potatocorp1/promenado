<?php

namespace Tests\Feature\Views;

use App\Models\Path;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PathViewTest extends TestCase
{
    use RefreshDatabase;

    private function createBasicUser(): User
    {
        /** @var mixed $user */
        return User::factory()->create(['is_admin' => 0]);
    }

    private function createAdminUser(): User
    {
        /** @var mixed $user */
        return User::factory()->create(['is_admin' => 1]);
    }

    /**
     * Button path creation access on list
     */

    public function test_guest_can_not_see_path_button_creation(): void
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertDontSee('Ajouter un parcours');
    }

    public function test_basic_user_can_not_see_path_button_creation(): void
    {
        $user = $this->createBasicUser();
        $response = $this->actingAs($user)->get('/');
        $response->assertStatus(200);
        $response->assertDontSee('Ajouter un parcours');
    }

    public function test_admin_can_see_path_button_creation(): void
    {
        $user = $this->createAdminUser();
        $response = $this->actingAs($user)->get('/');
        $response->assertStatus(200);
        $response->assertSee('Ajouter un parcours');
    }

    /**
     * Button path management access on list
     */

    public function test_guest_can_not_see_path_button_management(): void
    {
        $path = Path::factory()->create();
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertDontSee('Gérer');
    }

    public function test_basic_user_can_not_see_path_button_management(): void
    {
        $user = $this->createBasicUser();
        $response = $this->actingAs($user)->get('/');
        $response->assertStatus(200);
        $response->assertDontSee('Gérer');
    }

    public function test_admin_can_see_path_button_management(): void
    {
        Path::factory()->create();
        $user = $this->createAdminUser();
        $response = $this->actingAs($user)->get('/');
        $response->assertStatus(200);
        $response->assertSee('Gérer');
    }

    /**
     * Button step creation access on admin path view
     */

    public function test_admin_can_see_step_button_creation(): void
    {
        $path = Path::factory()->create();
        $user = $this->createAdminUser();
        $response = $this->actingAs($user)->get('/admin/path/'.$path->id);
        $response->assertStatus(200);
        $response->assertSee('Ajouter une étape');
    }

    /**
     * Button step update access on admin path view
     */

    public function test_admin_can_see_step_button_update(): void
    {
        $path = Path::factory()->create();
        $user = $this->createAdminUser();
        $response = $this->actingAs($user)->get('/admin/path/'.$path->id);
        $response->assertStatus(200);
        $response->assertSee('Modifier');
    }

    /**
     * Public status for path
     */

    public function test_guest_can_not_see_not_public_path(): void
    {
        $path = Path::factory()->create(['name' => 'Draft path example', 'is_public' => false]);
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertDontSee('Draft path example');

        $path->is_public = true;
        $path->save();
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSee('Draft path example');
    }

    public function test_basic_user_can_not_see_not_public_path(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create(['name' => 'Draft path example', 'is_public' => false]);
        $response = $this->actingAs($user)->get('/');
        $response->assertStatus(200);
        $response->assertDontSee('Draft path example');

        $path->is_public = true;
        $path->save();
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSee('Draft path example');
    }

    public function test_admin_can_see_not_public_path(): void
    {
        $user = $this->createAdminUser();
        Path::factory()->create(['name' => 'Draft path example', 'is_public' => false]);
        $response = $this->actingAs($user)->get('/');
        $response->assertStatus(200);
        $response->assertSee('Draft path example');
    }
}
