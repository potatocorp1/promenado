<?php

namespace Tests\Feature\Views;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StepViewTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_see_hint_link(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => 'foo'
        ]);
        $response = $this->actingAs($user)->get(route('step.show', $step->id));
        $response->assertStatus(200);
        $response->assertSeeText('Cliquez ici pour obtenir un indice');
    }

    public function test_user_can_not_see_hint_link_when_step_has_no_hint(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => ''
        ]);
        $response = $this->actingAs($user)->get(route('step.show', $step->id));
        $response->assertStatus(200);
        $response->assertDontSeeText('Cliquez ici pour obtenir un indice');
    }
}
