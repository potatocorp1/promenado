<?php

namespace Tests\Feature\Models;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_have_multiple_paths(): void
    {
        $user = User::factory()->hasAttached(
            Path::factory()->count(3)
        )->create();
        $this->assertCount(3, $user->paths);
    }

    public function test_user_can_have_multiple_steps(): void
    {
        $path = Path::factory()->create();
        $user = User::factory()->hasAttached(
            Step::factory()->count(3)->create(['path_id' => $path->id])
        )->create();
        $this->assertCount(3, $user->steps);
    }
}
