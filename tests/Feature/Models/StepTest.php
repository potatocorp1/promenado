<?php

namespace Tests\Feature\Models;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StepTest extends TestCase
{
    use RefreshDatabase;

    public function test_step_can_be_completed_by_many_users(): void
    {
        $path = Path::factory()->create();
        $step = Step::factory()->hasAttached(
            User::factory()->count(3)
        )->create(['path_id' => $path->id]);

        $this->assertCount(3, $step->users);
    }

    public function test_step_is_completed_for_a_given_user(): void
    {
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $step->users()->attach($user1->id, ['completed_at' => now()]);
        $this->assertTrue($step->isCompletedForUser($user1));
        $this->assertFalse($step->isCompletedForUser($user2));
    }

    public function test_hint_is_hidden_when_empty(): void
    {
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => ''
        ]);
        $this->assertFalse($step->hintIsDisplayedFor($user));
    }

    public function test_hint_is_displayed_when_user_asked_for_it(): void
    {
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => 'foo'
        ]);
        $step->users()->attach($user->id, ['hint_used' => true ]);
        $this->assertTrue($step->hintIsDisplayedFor($user));
    }

    public function test_hint_is_hidden_when_user_did_not_ask_for_it(): void
    {
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => 'foo'
        ]);
        $this->assertFalse($step->hintIsDisplayedFor($user));
    }

    public function test_hint_can_not_be_requested_when_empty(): void
    {
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => ''
        ]);
        $this->assertFalse($step->hintCanBeRequestedBy($user));
    }

    public function test_hint_can_be_requested_when_not_empty(): void
    {
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => 'foo'
        ]);
        $this->assertTrue($step->hintCanBeRequestedBy($user));
    }

    public function test_hint_can_not_be_requested_twice(): void
    {
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => 'foo'
        ]);
        $step->users()->attach($user->id, ['hint_used' => true ]);
        $this->assertFalse($step->hintCanBeRequestedBy($user));
    }

    public function test_step_is_not_completed_when_user_just_asked_for_an_hint(): void
    {
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => 'foo'
        ]);
        $step->users()->attach($user->id, ['hint_used' => true ]);
        $this->assertFalse($step->isCompletedForUser($user));
    }

    public function test_question_is_not_answered(): void
    {
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => 'foo'
        ]);
        $step->users()->attach($user->id);

        $this->assertFalse($step->questionIsAlreadyAnsweredBy($user));
    }

    public function test_question_is_answered(): void
    {
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => 'foo'
        ]);
        $step->users()->attach($user->id, [
            'correctly_answered_at' => now()
        ]);

        $this->assertTrue($step->questionIsAlreadyAnsweredBy($user));
    }

    public function test_question_nb_attemps(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'question' => 'What is the question?',
            'answer_options' => ['foo', 'bar', 'baz'],
            'answer_index' => 1
        ]);
        $step->users()->attach($user->id);

        $this->assertEquals($step->questionNbAttempsFor($user), 0);

        $this->actingAs($user)->post(route('step.answer-bonus-question', $step->id), ['answer_index' => 2]);
        $this->assertEquals($step->questionNbAttempsFor($user), 1);

        $this->actingAs($user)->post(route('step.answer-bonus-question', $step->id), ['answer_index' => 1]);
        $this->assertEquals($step->questionNbAttempsFor($user), 2);
    }

    public function test_question_nb_attemps_on_newly_added_step(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create();
        $path = Path::factory()->create();

        //case: step added after the user finished the whole path, no attach between step and user
        //function called for scoring page display
        $step = Step::factory()->create([
            'path_id' => $path->id
        ]);

        $this->assertEquals($step->questionNbAttempsFor($user), 0);
    }
}
