<?php

namespace Tests\Feature\Models;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PathTest extends TestCase
{
    use RefreshDatabase;

    public function test_path_can_have_multiple_steps(): void
    {
        $path = Path::factory()->create();
        Step::factory()->count(3)->create(['path_id' => $path->id]);

        $this->assertCount(3, $path->steps);
    }

    public function test_path_without_steps_has_a_progression_to_zero(): void
    {
        $path = Path::factory()->create();
        $user = User::factory()->create();
        $this->assertEquals($path->getProgressionPercentage($user), 0);
    }

    public function test_path_progression_is_zero_at_the_beginning(): void
    {
        $path = Path::factory()->create();
        $user = User::factory()->create();
        Step::factory()->count(3)->create(['path_id' => $path->id]);
        $this->assertEquals($path->getProgressionPercentage($user), 0);
    }

    public function test_path_progression_is_100_at_the_end(): void
    {
        $path = Path::factory()->create();
        /** @var mixed $user */
        $user = User::factory()->create();
        Step::factory()->count(3)->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        foreach ($path->steps as $step) {
            $this->actingAs($user)->get('/step/'.$step->id.'/complete/AZERTY');
        }
        $this->assertEquals($path->getProgressionPercentage($user), 100);
    }

    public function test_path_progression_is_in_between_during_progress(): void
    {
        $path = Path::factory()->create();
        /** @var mixed $user */
        $user = User::factory()->create();
        Step::factory()->count(3)->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $firstStep = Step::first();
        $this->actingAs($user)->get('/step/'.$firstStep->id.'/complete/AZERTY');
        $this->assertEquals($path->getProgressionPercentage($user), 33);
    }

    public function test_path_is_completed_for_a_given_user(): void
    {
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();
        $path = Path::factory()->create();
        $path->users()->attach($user1->id, ['completed_at' => now()]);
        $this->assertEquals($path->getStatusForUser($user1), 'completed');
        $this->assertEquals($path->getStatusForUser($user2), 'not_started');
    }

    public function test_new_path_next_step_is_the_first_one(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $this->assertEquals($path->getNextStep($user)->id, $step->id);
    }

    public function test_next_step_use_custom_step_ordering(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create();
        $path = Path::factory()->create();

        $step1 = Step::factory()->create(['path_id' => $path->id]);
        $step2 = Step::factory()->create(['path_id' => $path->id]);
        $path->update([
            'steps_order' => [$step2->id, $step1->id]
        ]);
        $path->save();

        $this->assertEquals($path->getNextStep($user)->id, $step2->id);
    }

    public function test_in_progress_path_next_step_is_the_first_not_completed(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create();
        $path = Path::factory()->create();

        $step1 = Step::factory()->create(['path_id' => $path->id]);
        $step1->users()->attach($user->id, [
            'completed_at' => now()
        ]);

        $step2 = Step::factory()->create(['path_id' => $path->id]);

        $this->assertEquals($path->getNextStep($user)->id, $step2->id);
    }

    public function test_sort_steps(): void
    {
        $path = Path::factory()->create(['steps_order' => [3, 1, 2]]);
        Step::factory()->count(3)->create(['path_id' => $path->id]);

        $this->assertEquals($path->steps->first()->id, 1);

        $path->sortSteps();
        $this->assertEquals($path->steps->first()->id, 3);
    }

    public function test_sort_steps_by_years(): void
    {
        $path = Path::factory()->create(['timeline' => ['years' => [
            1 => 1982,
            2 => 1250,
            3 => 1999
        ]]]);
        Step::factory()->count(3)->create(['path_id' => $path->id]);

        $this->assertEquals($path->steps->first()->id, 1);

        $path->sortStepsByYears();
        $this->assertEquals($path->steps->first()->id, 2);
    }

    public function test_qr_code_path_format(): void
    {
        $path = Path::factory()->create();
        $this->assertStringContainsString('storage/qrcodes/path_1.png', $path->qrcode());
    }

    public function test_path_has_no_timeline(): void
    {
        $path = Path::factory()->create();
        $this->assertEquals($path->hasTimeline(), false);
    }

    public function test_path_has_an_partial_timeline(): void
    {
        $path = Path::factory()->create(['timeline' => ['years' => [
            1 => 1982,
            2 => 1250
        ]]]);
        Step::factory()->count(3)->create(['path_id' => $path->id]);
        $this->assertEquals($path->hasTimeline(), true);
    }

    public function test_path_has_a_complete_timeline(): void
    {
        $path = Path::factory()->create(['timeline' => ['years' => [
            1 => 1982,
            2 => 1250,
            3 => 1999
        ]]]);
        Step::factory()->count(3)->create(['path_id' => $path->id]);
        $this->assertEquals($path->hasTimeline(), true);
    }

    public function test_path_has_an_incorrect_timeline_with_deleted_step(): void
    {
        $path = Path::factory()->create(['timeline' => ['years' => [
            1 => 1982,
            2 => 1250,
            3 => 1251,
            99 => 1999
        ]]]);
        Step::factory()->count(3)->create(['path_id' => $path->id]);
        $this->assertEquals($path->hasTimeline(), false);
    }

    public function test_timeline_is_completed_for_a_given_user(): void
    {
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();
        $path = Path::factory()->create();
        $path->users()->attach($user1->id, ['timeline_completed_at' => now()]);
        $this->assertEquals($path->timelineCompletedBy($user1), true);
        $this->assertEquals($path->timelineCompletedBy($user2), false);
    }
}
