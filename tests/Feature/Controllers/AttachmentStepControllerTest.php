<?php

namespace Tests\Feature\Controllers;

use App\Models\Attachment;
use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class AttachmentStepControllerTest extends TestCase
{
    use RefreshDatabase;

    private function callController($data = [])
    {
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        return $this->callControllerForStep($step, $data);
    }

    private function callControllerForStep(Step $step, $data = [])
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 1]);
        $file = UploadedFile::fake()->image('example.jpg');
        $default = [
            'attachable_type' => Step::class,
            'attachable_id' => $step->id,
            'image' => $file
        ];
        return $this->actingAs($user)->postJson(route('attachments.store'), array_merge($default, $data));
    }

    private function getFileForAttachment($attachment)
    {
        return storage_path('app/public/uploads/'.$attachment['name']);
    }

    public function test_correct_attachment_storage()
    {
        $response = $this->callController();
        $attachment = $response->json();
        $response->assertJsonStructure(['id', 'url']);
        self::assertFileExists($this->getFileForAttachment($attachment));
        $this->assertStringContainsString('/uploads/', $attachment['url']);
        $response->assertStatus(200);
    }

    public function test_delete_attachment_delete_file()
    {
        $response = $this->callController();
        $attachment = $response->json();
        self::assertFileExists($this->getFileForAttachment($attachment));
        Attachment::find($attachment['id'])->delete();
        self::assertFileDoesNotExist($this->getFileForAttachment($attachment));
    }

    public function test_delete_step_delete_all_attachments()
    {
        $response = $this->callController();
        $attachment = $response->json();
        Attachment::factory()->count(3)->create();
        self::assertFileExists($this->getFileForAttachment($attachment));
        $this->assertEquals(4, Attachment::count());
        Step::first()->delete();
        $this->assertEquals(3, Attachment::count());
        self::assertFileDoesNotExist($this->getFileForAttachment($attachment));
    }

    public function test_change_step_content_attachments_are_deleted()
    {
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $attachmentItinerary = $this->callControllerForStep($step)->json();
        $attachmentOnSite = $this->callControllerForStep($step)->json();

        Attachment::factory()->count(2)->create();
        self::assertFileExists($this->getFileForAttachment($attachmentItinerary));
        self::assertFileExists($this->getFileForAttachment($attachmentOnSite));
        $this->assertEquals(4, Attachment::count());

        $step->itinerary = "<img src=\"#{$attachmentItinerary['url']}\" /> and random text";
        $step->on_site = "<img src=\"#{$attachmentOnSite['url']}\" /> and random text";
        $step->save();
        $this->assertEquals(4, Attachment::count());

        $step->itinerary = "just random text";
        $step->on_site = "just random text";
        $step->save();
        $this->assertEquals(2, Attachment::count());
        self::assertFileDoesNotExist($this->getFileForAttachment($attachmentItinerary));
        self::assertFileDoesNotExist($this->getFileForAttachment($attachmentOnSite));
    }

    public function test_change_step_content_only_concerned_attachments_are_deleted()
    {
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $attachmentItinerary = $this->callControllerForStep($step)->json();
        $attachmentOnSite = $this->callControllerForStep($step)->json();
        $this->assertEquals(2, Attachment::count());

        $step->itinerary = "just random text";
        $step->on_site = "<img src=\"#{$attachmentOnSite['url']}\" /> and random text";
        $step->save();
        $this->assertEquals(1, Attachment::count());
        self::assertFileDoesNotExist($this->getFileForAttachment($attachmentItinerary));
        self::assertFileExists($this->getFileForAttachment($attachmentOnSite));
    }

    public function test_change_step_content_attachments_are_deleted_if_image_changed()
    {
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $attachmentItinerary = $this->callControllerForStep($step)->json();
        $attachmentOnSite = $this->callControllerForStep($step)->json();

        Attachment::factory()->count(2)->create();
        self::assertFileExists($this->getFileForAttachment($attachmentItinerary));
        self::assertFileExists($this->getFileForAttachment($attachmentOnSite));
        $this->assertEquals(4, Attachment::count());


        $step->itinerary = "<img src=\"#{$attachmentItinerary['url']}\" /> and random text";
        $step->on_site = "<img src=\"#{$attachmentOnSite['url']}\" /> and random text";
        $step->save();
        $this->assertEquals(4, Attachment::count());

        $step->itinerary = "<img src=\"another_image.jpg\" /> and random text";
        $step->on_site = "<img src=\"yet_another_image.jpg\" /> and random text";
        $step->save();
        $this->assertEquals(2, Attachment::count());
        self::assertFileDoesNotExist($this->getFileForAttachment($attachmentItinerary));
        self::assertFileDoesNotExist($this->getFileForAttachment($attachmentOnSite));
    }
}
