<?php

namespace Tests\Feature\Controllers;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CodeValidationTest extends TestCase
{
    use RefreshDatabase;

    private function createBasicUser(): User
    {
        /** @var mixed $user */
        return User::factory()->create(['is_admin' => 0]);
    }

    private function createAdminUser(): User
    {
        /** @var mixed $user */
        return User::factory()->create(['is_admin' => 1]);
    }


    public function test_secret_code_is_case_insensitive(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'Croix']);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/croix');
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_secret_code_with_accent(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'Château']);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/chateau');
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_secret_code_is_case_insensitive_with_accent(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'Église']);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/eglise');
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_secret_code_accent_encoded(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'Château']);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/Ch%C3%A2teau');
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }
}
