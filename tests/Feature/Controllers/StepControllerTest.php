<?php

namespace Tests\Feature\Controllers;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class StepControllerTest extends TestCase
{
    use RefreshDatabase;

    private function createBasicUser(): User
    {
        /** @var mixed $user */
        return User::factory()->create(['is_admin' => 0]);
    }

    private function createAdminUser(): User
    {
        /** @var mixed $user */
        return User::factory()->create(['is_admin' => 1]);
    }


    /**
     * Step deletion
     */

    public function test_step_can_be_deleted(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $response = $this->actingAs($user)->delete('/step/' . $step->id);
        $this->assertModelMissing($step);
        $response->assertStatus(302);
        $response->assertRedirect('admin/path/'.$path->id);
    }

    /**
     * Step update process
     */

    public function test_step_can_be_updated(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'name' => 'foo',
        ]);
        $newStep = [
            'path_id' => $path->id,
            'name' => 'new name',
            'code' => 'new code',
            'itinerary' => 'new itinerary',
            'on_site' => 'new on site text',
        ];
        $response = $this->actingAs($user)->put('/step/' . $step->id, $newStep);
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('steps', [
            'id' => $step->id,
            'path_id' => $path->id,
            'name' => 'new name',
            'code' => 'new code',
            'itinerary' => 'new itinerary',
            'on_site' => 'new on site text',
        ]);
        $response->assertRedirect('admin/path/'.$path->id);
    }

    public function test_step_can_have_a_new_cover(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'name' => 'foo',
        ]);

        $file = UploadedFile::fake()->image('example.jpg');
        $newStep = [
            'path_id' => $path->id,
            'name' => 'new name',
            'code' => 'new code',
            'itinerary' => 'new itinerary',
            'on_site' => 'new on site text',
            'cover' => $file
        ];

        $response = $this->actingAs($user)->put('/step/' . $step->id, $newStep);

        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('steps', [
            'id' => $step->id,
            'path_id' => $path->id,
            'cover' => 'covers/step_1.jpg'
        ]);
        $response->assertRedirect('admin/path/'.$path->id);
    }

    public function test_answers_options_can_not_be_empty_if_there_is_a_question(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'name' => 'foo',
        ]);
        $newStep = [
            'path_id' => $path->id,
            'name' => 'new name',
            'code' => 'new code',
            'itinerary' => 'new itinerary',
            'on_site' => 'new on site text',
            'question' => 'an interesting question'
        ];
        $response = $this->actingAs($user)->put('/step/' . $step->id, $newStep);
        $response->assertStatus(302);
        $response->assertSessionHasErrors();
        $response->assertInvalid(['answer_options']);
    }

    public function test_all_answers_options_must_be_filled_if_there_is_a_question(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'name' => 'foo',
        ]);
        $newStep = [
            'path_id' => $path->id,
            'name' => 'new name',
            'code' => 'new code',
            'itinerary' => 'new itinerary',
            'on_site' => 'new on site text',
            'question' => 'an interesting question',
            'answer_options' => ['answer A', 'answer B', '']
        ];
        $response = $this->actingAs($user)->put('/step/' . $step->id, $newStep);
        $response->assertStatus(302);
        $response->assertSessionHasErrors();
        $response->assertInvalid(['answer_options.2']);
    }

    public function test_answer_index_can_not_be_negative(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'name' => 'foo',
        ]);
        $newStep = [
            'path_id' => $path->id,
            'name' => 'new name',
            'code' => 'new code',
            'itinerary' => 'new itinerary',
            'on_site' => 'new on site text',
            'answer_index' => -1
        ];
        $response = $this->actingAs($user)->put('/step/' . $step->id, $newStep);
        $response->assertStatus(302);
        $response->assertSessionHasErrors();
        $response->assertInvalid(['answer_index']);
    }

    public function test_answer_index_can_not_be_higher_than_2(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'name' => 'foo',
        ]);
        $newStep = [
            'path_id' => $path->id,
            'name' => 'new name',
            'code' => 'new code',
            'itinerary' => 'new itinerary',
            'on_site' => 'new on site text',
            'answer_index' => 99
        ];
        $response = $this->actingAs($user)->put('/step/' . $step->id, $newStep);
        $response->assertStatus(302);
        $response->assertSessionHasErrors();
        $response->assertInvalid(['answer_index']);
    }

    public function test_answers_options_are_stored_null_if_there_is_not_question(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'name' => 'foo',
        ]);
        $newStep = [
            'path_id' => $path->id,
            'name' => 'new name',
            'code' => 'new code',
            'itinerary' => 'new itinerary',
            'on_site' => 'new on site text',
            'question' => '',
            'answer_options' => ['', '', '']
        ];
        $response = $this->actingAs($user)->put('/step/' . $step->id, $newStep);
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('steps', [
            'id' => $step->id,
            'path_id' => $path->id,
            'question' => null,
            'answer_options' => null
        ]);
    }

    public function test_bonus_question_can_be_stored(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'name' => 'foo',
        ]);
        $newStep = [
            'path_id' => $path->id,
            'name' => 'new name',
            'code' => 'new code',
            'itinerary' => 'new itinerary',
            'on_site' => 'new on site text',
            'question' => 'what is the secret number?',
            'answer_options' => ['1', '99', 'no one']
        ];
        $response = $this->actingAs($user)->put('/step/' . $step->id, $newStep);
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('steps', [
            'id' => $step->id,
            'path_id' => $path->id,
            'question' => 'what is the secret number?',
            'answer_options' => json_encode(['1', '99', 'no one'])
        ]);
    }

    /**
     * Hint mechaninism
     */

    public function test_can_have_an_optional_hint(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'name' => 'foo'
        ]);
        $step->hint = 'bar';
        $response = $this->actingAs($user)->put(route('step.update', $step->id), $step->getAttributes());
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('steps', [
            'id' => $step->id,
            'path_id' => $path->id,
            'name' => 'foo',
            'hint' => 'bar',
        ]);
    }

    public function test_hint_is_not_displayed_in_the_first_time(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => 'super hint for the step'
        ]);
        $response = $this->actingAs($user)->get(route('step.show', $step));
        $response->assertStatus(200);
        $response->assertDontSeeText('super hint for the step');
    }

    public function test_user_can_ask_for_hint(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'hint' => 'super hint for the step'
        ]);
        $response = $this->actingAs($user)->followingRedirects()->post(route('step.ask-for-hint', $step->id));
        $response->assertStatus(200);
        $log = $step->users()->firstWhere('user_id', $user->id)->pivot->hint_used;
        $response->assertSeeText('super hint for the step');
        $this->assertEquals($log, 1);
    }

    /**
     * Step completion
     */

    public function test_user_can_complete_a_step_with_right_code(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $response = $this->actingAs($user)->post('/step/'.$step->id.'/complete', ['code' => 'AZERTY']);
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_user_can_not_complete_a_step_with_empty_code(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $response = $this->actingAs($user)->post('/step/'.$step->id.'/complete', ['code' => '']);
        $response->assertStatus(302);
        $response->assertRedirect(route('step.show', $step));
        $this->assertDatabaseMissing('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_user_can_not_complete_a_step_with_wrong_code(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $response = $this->actingAs($user)->post('/step/'.$step->id.'/complete', ['code' => 'AAAAAA']);
        $response->assertStatus(302);
        $response->assertRedirect(route('step.show', $step));
        $this->assertDatabaseMissing('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_user_can_complete_a_step_by_url_with_right_code(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/AZERTY');
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_user_can_not_complete_a_step_by_url_with_wrong_code(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/AAAAAA');
        $response->assertStatus(302);
        $response->assertRedirect(route('step.show', $step));
        $this->assertDatabaseMissing('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    /**
     * Step bonus question
     */

    public function test_user_can_answer_bonus_question(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'question' => 'What is the question?',
            'answer_options' => ['foo', 'bar', 'baz'],
            'answer_index' => 1
        ]);
        $step->users()->attach($user->id);

        $response = $this->actingAs($user)->post(route('step.answer-bonus-question', $step->id), ['answer_index' => 1]);
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $log = $step->users()->firstWhere('user_id', $user->id)->pivot;
        $this->assertNotNull($log->correctly_answered_at);
        $this->assertEquals($log->question_nb_attempts, 1);
    }

    public function test_user_cant_answer_bonus_question_with_wrong_answer(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'question' => 'What is the question?',
            'answer_options' => ['foo', 'bar', 'baz'],
            'answer_index' => 1
        ]);
        $step->users()->attach($user->id);

        $response = $this->actingAs($user)->post(route('step.answer-bonus-question', $step->id), ['answer_index' => 0]);
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $log = $step->users()->firstWhere('user_id', $user->id)->pivot;
        $this->assertNull($log->correctly_answered_at);
        $this->assertEquals($log->question_nb_attempts, 1);
    }

    public function test_number_attempts_is_incremented_at_each_try(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create([
            'path_id' => $path->id,
            'question' => 'What is the question?',
            'answer_options' => ['foo', 'bar', 'baz'],
            'answer_index' => 1
        ]);
        $step->users()->attach($user->id);

        $this->actingAs($user)->post(route('step.answer-bonus-question', $step->id), ['answer_index' => 0]);
        $this->actingAs($user)->post(route('step.answer-bonus-question', $step->id), ['answer_index' => 0]);
        $this->actingAs($user)->post(route('step.answer-bonus-question', $step->id), ['answer_index' => 0]);

        $log = $step->users()->firstWhere('user_id', $user->id)->pivot;
        $this->assertEquals($log->question_nb_attempts, 3);
    }
}
