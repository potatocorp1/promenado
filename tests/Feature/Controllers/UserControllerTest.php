<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_see_new_name_form(): void
    {
        $user = User::factory()->create();
        /** @var mixed $user */
        $response = $this->actingAs($user)->get(route('user.edit-name'));
        $response->assertStatus(200);
        $response->assertSee('Nouveau nom');
    }

    public function test_user_can_change_name(): void
    {
        $user = User::factory()->create(['name' => 'John']);
        /** @var mixed $user */
        $response = $this->actingAs($user)->post(route('user.update-name', ['name' => 'Tom']));
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => 'Tom',
        ]);
    }

    public function test_user_can_delete_his_account(): void
    {
        $user = User::factory()->create();
        /** @var mixed $user */
        $response = $this->actingAs($user)->delete(route('user.destroy', ['user' => $user]));
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseMissing('users', [
            'id' => $user->id
        ]);
    }

    public function test_user_can_not_delete_another_user(): void
    {
        $user = User::factory()->create();
        $anotherUser = User::factory()->create();
        /** @var mixed $user */
        $response = $this->actingAs($user)->delete(route('user.destroy', ['user' => $anotherUser]));
        $response->assertStatus(403);
        $this->assertDatabaseHas('users', [
            'id' => $anotherUser->id
        ]);
    }

    public function test_user_can_see_new_password_form(): void
    {
        $user = User::factory()->create();
        /** @var mixed $user */
        $response = $this->actingAs($user)->get(route('user.edit-password'));
        $response->assertStatus(200);
        $response->assertSee('Nouveau mot de passe');
    }

    public function test_user_can_not_change_password_without_old_one(): void
    {
        $oldPasswordHash = Hash::make('oldpassword');
        $user = User::factory()->create(['password' => $oldPasswordHash]);
        /** @var mixed $user */
        $response = $this->actingAs($user)->post(route('user.update-password', [
            'oldPassword' => 'wrongpassword',
            'password' => 'newpassword',
            'password_confirmation' => 'newpassword'
        ]));
        $response->assertStatus(302);
        $response->assertSessionHas('status_warning');
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'password' => $oldPasswordHash,
        ]);
    }

    public function test_user_can_change_password(): void
    {
        $oldPasswordHash = Hash::make('oldpassword');
        $user = User::factory()->create(['password' => $oldPasswordHash]);
        /** @var mixed $user */
        $response = $this->actingAs($user)->post(route('user.update-password', [
            'oldPassword' => 'oldpassword',
            'password' => 'newpassword',
            'password_confirmation' => 'newpassword'
        ]));
        $response->assertStatus(302);
        $response->assertSessionMissing('status_warning');
        $userUpdated = User::find($user->id);
        $this->assertNotEquals($userUpdated->password, $oldPasswordHash);
    }
}
