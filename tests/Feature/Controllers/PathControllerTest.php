<?php

namespace Tests\Feature\Controllers;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class PathControllerTest extends TestCase
{
    use RefreshDatabase;

    private function createBasicUser(): User
    {
        /** @var mixed $user */
        return User::factory()->create(['is_admin' => 0]);
    }

    private function createAdminUser(): User
    {
        /** @var mixed $user */
        return User::factory()->create(['is_admin' => 1]);
    }

    /**
     * Path deletion process
     */

    public function test_empty_path_can_be_deleted(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $response = $this->actingAs($user)->delete('/path/' . $path->id);
        $this->assertModelMissing($path);
        $response->assertStatus(302);
    }

    public function test_path_can_be_deleted_with_all_its_steps(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $steps = Step::factory()->count(3)->create(['path_id' => $path->id]);
        $response = $this->actingAs($user)->delete('/path/' . $path->id);
        $this->assertModelMissing($path);
        $this->assertModelMissing($steps[0]);
        $response->assertStatus(302);
        $response->assertRedirect('/');
    }

    /**
     * Path update process
     */

    public function test_path_can_not_be_updated_empty(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $newPath = [
            'name' => '',
            'duration' => '',
            'description' => '',
            'start_place' => '',
        ];
        $response = $this->actingAs($user)->put('/path/' . $path->id, $newPath);
        $response->assertInvalid(['name', 'duration', 'description', 'start_place']);
        $response->assertStatus(302);
    }

    public function test_path_can_be_updated(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $newPath = [
            'name' => 'new name',
            'duration' => 'new duration',
            'description' => 'new description',
            'start_place' => 'new place',
        ];
        $response = $this->actingAs($user)->put('/path/' . $path->id, $newPath);
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'name' => 'new name',
            'duration' => 'new duration',
            'description' => 'new description',
            'start_place' => 'new place',
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('admin/path/'.$path->id);
    }

    public function test_path_can_have_a_new_cover(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();

        $file = UploadedFile::fake()->image('example.jpg');
        $newPath = [
            'name' => 'new name',
            'duration' => 'new duration',
            'description' => 'new description',
            'start_place' => 'new place',
            'cover' => $file
        ];
        $response = $this->actingAs($user)->put('/path/' . $path->id, $newPath);
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'cover' => 'covers/path_1.jpg'
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('admin/path/'.$path->id);
    }

    /**
     * Path public status management
     */

    public function test_admin_can_activate_a_path(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create(['is_public' => false]);
        $response = $this->actingAs($user)->put(route('path.toggle', $path->id));
        $response->assertStatus(302);
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'is_public' => true,
        ]);
    }

    public function test_admin_can_desactivate_a_path(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create(['is_public' => true]);
        $response = $this->actingAs($user)->put(route('path.toggle', $path->id));
        $response->assertStatus(302);
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'is_public' => false,
        ]);
    }

    /**
     * Path start process
     */

    public function test_basic_user_can_start_a_path(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        Step::factory()->create(['path_id' => $path->id, 'name' => 'First step']);
        $response = $this->actingAs($user)->followingRedirects()->post(route('path.start', $path->id));
        $response->assertStatus(200);
        $this->assertDatabaseHas('path_user', [
            'path_id' => $path->id,
            'user_id' => $user->id,
            'completed_at' => null,
        ]);
        $response->assertSee('First step');
    }

    /**
     * Path restart process
     */

    public function test_basic_user_can_restart_a_path(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        Step::factory()->count(3)->create(['path_id' => $path->id, 'code' => 'AZERTY']);

        $this->actingAs($user)->post(route('path.start', $path));
        foreach ($path->steps as $step) {
            $this->actingAs($user)->get('/step/'.$step->id.'/complete/AZERTY');
        }

        $response = $this->actingAs($user)->post(route('path.restart', $path->id));

        $this->assertDatabaseMissing('step_user', [
            'step_id' => 1,
            'user_id' => $user->id
        ]);
        $this->assertDatabaseMissing('path_user', [
            'path_id' => $path->id,
            'user_id' => $user->id
        ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('path.show', $path->id));
    }

    /**
     * Path end process
     */

    public function test_end_path_button_is_visible_when_all_steps_are_completed(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        Step::factory()->count(3)->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        foreach ($path->steps as $step) {
            $this->actingAs($user)->get('/step/'.$step->id.'/complete/AZERTY');
        }
        $response = $this->followingRedirects()->post(route('path.start', $path));
        $response->assertSee('Terminer le parcours');
    }

    public function test_end_path_button_not_visible_when_all_steps_not_completed(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        Step::factory()->count(3)->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $firstStep = Step::first();
        $this->actingAs($user)->get('/step/'.$firstStep->id.'/complete/AZERTY');

        $response = $this->followingRedirects()->post(route('path.start', $path));
        $response->assertDontSee('Terminer le parcours');
    }

    public function test_user_can_end_a_path(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        Step::factory()->count(3)->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $this->actingAs($user)->post(route('path.start', $path));
        foreach ($path->steps as $step) {
            $this->actingAs($user)->get('/step/'.$step->id.'/complete/AZERTY');
        }
        $response = $this->actingAs($user)->post(route('path.end', $path));
        $response->assertStatus(302);
        $log = $path->users()->firstWhere('user_id', $user->id)->pivot->completed_at;
        $this->assertNotNull($log);
    }

    public function test_user_can_not_end_a_path_with_incomplete_steps(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        Step::factory()->count(3)->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $this->actingAs($user)->post(route('path.start', $path));
        $firstStep = Step::first();
        $this->actingAs($user)->get('/step/'.$firstStep->id.'/complete/AZERTY');

        $response = $this->actingAs($user)->post(route('path.end', $path));
        $response->assertStatus(302);
        $response->assertRedirect(route('path.start', $path));
    }

    public function test_completion_date_is_not_overriden(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        Step::factory()->count(1)->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $firstStep = Step::first();
        $this->actingAs($user)->get('/step/'.$firstStep->id.'/complete/AZERTY');

        $path->users()->attach($user->id, ['completed_at' => '01/01/2020' ]);
        $firstLog = $path->users()->firstWhere('user_id', $user->id)->pivot->completed_at;

        $this->actingAs($user)->get('/path/'.$path->id.'/end');
        $secondLog = $path->users()->firstWhere('user_id', $user->id)->pivot->completed_at;

        $this->assertEquals($firstLog, $secondLog);
    }

    private function createStep(int $pathId, User $user, string $name): int
    {
        $step = Step::factory()->create([
            'path_id' => $pathId
        ]);
        $step->update([
            'name' => $name,
            'code' => $name,
            'itinerary' => 'new itinerary',
            'on_site' => 'new on site text',
        ]);
        $this->actingAs($user)->put(route('step.update', $step->id), $step->getAttributes());
        return $step->id;
    }

    public function test_steps_order_is_updated_on_step_creation(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'steps_order' => null,
        ]);

        $step1 = $this->createStep($path->id, $user, 'step1');
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'steps_order' => json_encode([$step1]),
        ]);

        $step2 = $this->createStep($path->id, $user, 'step2');
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'steps_order' => json_encode([$step1, $step2]),
        ]);
    }

    public function test_steps_order_is_updated_on_step_deletion(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();

        $step1 = $this->createStep($path->id, $user, 'step1');
        $step2 = $this->createStep($path->id, $user, 'step2');
        $step3 = $this->createStep($path->id, $user, 'step3');
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'steps_order' => json_encode([$step1, $step2, $step3]),
        ]);

        $this->actingAs($user)->delete(route('step.destroy', $step2));
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'steps_order' => json_encode([$step1, $step3]),
        ]);
    }

    public function test_welcome_cta_is_first_public_path(): void
    {
        $pathPrivate = Path::factory()->create(['is_public' => false]);
        $response = $this->get(route('main'));
        $response->assertViewHas('ctaLink', '#');

        $pathPublic = Path::factory()->create(['is_public' => true]);
        $response = $this->get(route('main'));
        $response->assertViewHas('ctaLink', route('path.show', $pathPublic));

        $pathPublicNumber2 = Path::factory()->create(['is_public' => true]);
        $response = $this->get(route('main'));
        $response->assertViewHas('ctaLink', route('path.show', $pathPublic));
    }

    public function test_user_is_redirected_to_good_page_during_path_progression(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $this->actingAs($user)->post(route('path.start', $path));

        $response = $this->actingAs($user)->get(route('path.show.user', $path));
        $response->assertViewIs('path.ongoing');

        $this->actingAs($user)->get('/step/'.$step->id.'/complete/AZERTY');
        $response = $this->actingAs($user)->get(route('path.show.user', $path));
        $response->assertViewIs('path.ongoing');

        $response = $this->actingAs($user)->post(route('path.end', $path));
        $response = $this->actingAs($user)->get(route('path.show.user', $path));
        $response->assertViewIs('path.end');
    }

    public function test_timeline_can_be_set(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $steps = Step::factory()->count(2)->create(['path_id' => $path->id]);
        $timeline = ['year' => [1 => 1999, 2 => 1650]];

        $response = $this->actingAs($user)->put(route('path.update-timeline', $path->id), $timeline);
        $response->assertStatus(302);
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'timeline' => json_encode(['years' => [1 => 1999, 2 => 1650]])
        ]);
        $response->assertRedirect('admin/path/'.$path->id);
    }

    public function test_timeline_can_be_set_with_part_of_the_steps(): void
    {
        $user = $this->createAdminUser();
        $path = Path::factory()->create();
        $steps = Step::factory()->count(3)->create(['path_id' => $path->id]);
        $timeline = ['year' => [1 => 1999, 2 => 1650, 3 => ""]];

        $response = $this->actingAs($user)->put(route('path.update-timeline', $path->id), $timeline);
        $response->assertStatus(302);
        $this->assertDatabaseHas('paths', [
            'id' => $path->id,
            'timeline' => json_encode(['years' => [1 => 1999, 2 => 1650]])
        ]);
        $response->assertRedirect('admin/path/'.$path->id);
    }

    public function test_user_is_redirected_to_timeline_when_needed(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create(['timeline' => ['years' => [1 => 1999]]]);
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $this->actingAs($user)->post(route('path.start', $path));

        $response = $this->actingAs($user)->get(route('path.show.user', $path));
        $response->assertViewIs('path.ongoing');

        $this->actingAs($user)->get('/step/'.$step->id.'/complete/AZERTY');
        $response = $this->actingAs($user)->get(route('path.show.user', $path));
        $response->assertViewIs('path.ongoing');

        $response = $this->actingAs($user)->post(route('path.end', $path));
        $response = $this->actingAs($user)->get(route('path.show.user', $path));
        $response->assertViewIs('path.timeline');

        $response = $this->actingAs($user)->post(route('path.complete-timeline', $path));
        $response = $this->actingAs($user)->get(route('path.show.user', $path));
        $response->assertViewIs('path.end');
    }
}
