<?php

namespace Tests\Feature\Controllers;

use App\Models\Attachment;
use App\Models\Path;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class AttachmentPathControllerTest extends TestCase
{
    use RefreshDatabase;

    private function callController($data = [])
    {
        $path = Path::factory()->create();
        return $this->callControllerForPath($path, $data);
    }

    private function callControllerForPath(Path $path, $data = [])
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 1]);
        $file = UploadedFile::fake()->image('example.jpg');
        $default = [
            'attachable_type' => Path::class,
            'attachable_id' => $path->id,
            'image' => $file
        ];
        return $this->actingAs($user)->postJson(route('attachments.store'), array_merge($default, $data));
    }

    private function getFileForAttachment($attachment)
    {
        return storage_path('app/public/uploads/'.$attachment['name']);
    }

    public function test_correct_attachment_storage()
    {
        $response = $this->callController();
        $attachment = $response->json();
        $response->assertJsonStructure(['id', 'url']);
        self::assertFileExists($this->getFileForAttachment($attachment));
        $this->assertStringContainsString('/uploads/', $attachment['url']);
        $response->assertStatus(200);
    }

    public function test_delete_attachment_delete_file()
    {
        $response = $this->callController();
        $attachment = $response->json();
        self::assertFileExists($this->getFileForAttachment($attachment));
        Attachment::find($attachment['id'])->delete();
        self::assertFileDoesNotExist($this->getFileForAttachment($attachment));
    }

    public function test_delete_path_delete_all_attachments()
    {
        $response = $this->callController();
        $attachment = $response->json();
        Attachment::factory()->count(3)->create();
        self::assertFileExists($this->getFileForAttachment($attachment));
        $this->assertEquals(4, Attachment::count());
        Path::first()->delete();
        $this->assertEquals(3, Attachment::count());
        self::assertFileDoesNotExist($this->getFileForAttachment($attachment));
    }

    public function test_change_path_content_attachments_are_deleted()
    {
        $path = Path::factory()->create();
        $attachment = $this->callControllerForPath($path)->json();

        Attachment::factory()->count(2)->create();
        self::assertFileExists($this->getFileForAttachment($attachment));
        $this->assertEquals(3, Attachment::count());

        $path->description = "<img src=\"#{$attachment['url']}\" /> and random text";
        $path->save();
        $this->assertEquals(3, Attachment::count());

        $path->description = "just random text";
        $path->save();
        $this->assertEquals(2, Attachment::count());
        self::assertFileDoesNotExist($this->getFileForAttachment($attachment));
    }

    public function test_change_path_content_attachments_are_deleted_if_image_changed()
    {
        $path = Path::factory()->create();
        $attachment = $this->callControllerForPath($path)->json();

        Attachment::factory()->count(2)->create();
        self::assertFileExists($this->getFileForAttachment($attachment));
        $this->assertEquals(3, Attachment::count());


        $path->description = "<img src=\"#{$attachment['url']}\" /> and random text";
        $path->save();
        $this->assertEquals(3, Attachment::count());

        $path->description = "<img src=\"another_image.jpg\" /> and random text";
        $path->save();
        $this->assertEquals(2, Attachment::count());
        self::assertFileDoesNotExist($this->getFileForAttachment($attachment));
    }
}
