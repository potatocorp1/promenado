<?php

namespace Tests\Feature\Controllers;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class AttachmentControllerTest extends TestCase
{
    use RefreshDatabase;

    private function callController($data = [])
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 1]);
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id]);
        $file = UploadedFile::fake()->image('example.jpg');
        $default = [
            'attachable_type' => Step::class,
            'attachable_id' => $step->id,
            'image' => $file
        ];
        return $this->actingAs($user)->postJson(route('attachments.store'), array_merge($default, $data));
    }

    public function test_incorrect_image()
    {
        $response = $this->callController(['image' => 'image']);
        $response->assertJsonValidationErrorFor('image');
        $response->assertStatus(422);
    }

    public function test_incorrect_attachment_type()
    {
        $response = $this->callController(['attachable_type' => 'PPPPP']);
        $response->assertJsonStructure(['attachable_type']);
        $response->assertStatus(422);
    }

    public function test_incorrect_attachment_id()
    {
        $response = $this->callController(['attachable_id' => 3]);
        $response->assertJsonStructure(['attachable_id']);
        $response->assertStatus(422);
    }
}
