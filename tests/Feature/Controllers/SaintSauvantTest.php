<?php

namespace Tests\Feature\Controllers;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Tests for hardcoded fixes
 * Just for paths in Saint-Sauvant
 * Long path id = 2
 * Short path id = 12
 */
class SaintSauvantTest extends TestCase
{
    use RefreshDatabase;

    private function createBasicUser(): User
    {
        /** @var mixed $user */
        return User::factory()->create(['is_admin' => 0]);
    }

    private function createAdminUser(): User
    {
        /** @var mixed $user */
        return User::factory()->create(['is_admin' => 1]);
    }

    public function test_fix_physical_panel_29_chateau_xavier_bernard(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'Château', 'id' => 29]);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/Ch%C3%A2teau');
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_fix_physical_panel_only_for_29_chateau_xavier_bernard(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/Ch%C3%A2teau');
        $response->assertStatus(302);
        $this->assertDatabaseMissing('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_fix_physical_panel_34_chateau_eau(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => "Château d'eau", 'id' => 34]);
        $response = $this->actingAs($user)->followingRedirects()->post(route('path.start', $path->id));
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/Ch%C3%A2teau%20d%27eau');
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_fix_physical_panel_only_for_34_chateau_eau(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => 'AZERTY']);
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/Ch%C3%A2teau%20d%27eau');
        $response->assertStatus(302);
        $this->assertDatabaseMissing('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_fix_physical_panel_36_rue_aiguillettes_qrcode(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => "Rue", 'id' => 36]);
        $response = $this->actingAs($user)->followingRedirects()->post(route('path.start', $path->id));
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/Anguillettes');
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_fix_physical_panel_36_rue_aiguillettes_wrong_code(): void
    {
        $user = $this->createBasicUser();
        $path = Path::factory()->create();
        $step = Step::factory()->create(['path_id' => $path->id, 'code' => "Rue", 'id' => 36]);
        $response = $this->actingAs($user)->followingRedirects()->post(route('path.start', $path->id));
        $response = $this->actingAs($user)->get('/step/'.$step->id.'/complete/WrongCode');
        $response->assertStatus(302);
        $this->assertDatabaseMissing('step_user', [
            'step_id' => $step->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_fix_short_path_steps(): void
    {
        $user = $this->createBasicUser();
        $pathLong = Path::factory()->create();
        $pathShort = Path::factory()->create();
        Step::factory()->create(['path_id' => $pathShort->id, 'code' => 'azerty']);
        $this->actingAs($user)->followingRedirects()->post(route('path.start', $pathShort->id));
        $commonSteps = [
            26 => 69,
            27 => 68,
            29 => 67,
            36 => 78,
            46 => 53,
            47 => 52,
            48 => 51,
        ];
        foreach ($commonSteps as $longId => $shortId) {
            $stepLong = Step::factory()->create(['path_id' => $pathLong->id, 'code' => 'azerty', 'id' => $longId]);
            $stepShort = Step::factory()->create(['path_id' => $pathShort->id, 'code' => 'azerty', 'id' => $shortId]);
            $response = $this->actingAs($user)->get('/step/'.$stepLong->id.'/complete/azerty');
            $response->assertStatus(302);
            $response->assertSessionHasNoErrors();
            $response->assertRedirect(route('step.complete.by.url', [
                'step' => $stepShort,
                'code' => 'azerty'
            ]));
        }
    }

    public function test_ambigous_step_on_long_path(): void
    {
        $user = $this->createBasicUser();
        $pathLong = Path::factory()->create();
        $pathShort = Path::factory()->create();

        Step::factory()->create(['path_id' => $pathLong->id, 'code' => 'azerty']);
        $this->actingAs($user)->followingRedirects()->post(route('path.start', $pathLong->id));

        $ambigousStep = 26;
        $ambigousStepShortId = 69;

        $stepLong = Step::factory()->create(['path_id' => $pathLong->id, 'code' => 'azerty', 'id' => $ambigousStep]);
        $stepShort = Step::factory()->create(['path_id' => $pathShort->id, 'code' => 'azerty', 'id' => $ambigousStepShortId]);

        $response = $this->actingAs($user)->get('/step/'.$stepLong->id.'/complete/azerty');
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('step_user', [
            'step_id' => $stepLong->id,
            'user_id' => $user->id,
        ]);
    }
}
