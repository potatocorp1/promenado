<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class StatisticsControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_number_users_counts_all_users_type(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create(['is_admin' => 1]);

        $response = $this->actingAs($user)->get('/statistics');
        $response->assertViewHas('nbUsers', 1);

        User::factory()->count(3)->create(['is_admin' => 0]);
        $response = $this->actingAs($user)->get('/statistics');
        $response->assertViewHas('nbUsers', 4);
    }

    public function test_users_are_counted_on_register_month(): void
    {
        /** @var mixed $user */
        $user = User::factory()->create([
            'is_admin' => 1,
            'created_at' => Carbon::createFromDate(2022, 1, 10)
        ]);

        User::factory()->count(2)->create([
            'created_at' => Carbon::createFromDate(2022, 4, 10)
        ]);
        User::factory()->count(3)->create([
            'created_at' => Carbon::createFromDate(2022, 5, 2)
        ]);

        $response = $this->actingAs($user)->get('/statistics');
        $response->assertViewHas('nbUsersPerYearsAndMonths', [
            2022 => [
                1 => 1,
                2 => 0,
                3 => 0,
                4 => 2,
                5 => 3,
                6 => 0,
                7 => 0,
                8 => 0,
                9 => 0,
                10 => 0,
                11 => 0,
                12 => 0,
            ]
        ]);
    }
}
