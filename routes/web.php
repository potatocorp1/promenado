<?php

use App\Http\Controllers\AttachmentController;
use App\Http\Controllers\PathController;
use App\Http\Controllers\StatisticsController;
use App\Http\Controllers\StepController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PathController::class, 'index'])
    ->name('main');
Route::get('/about', function () {
    return view('about');
});
Route::get('/mentions-legales', function () {
    return view('legals');
})->name('legals');
Route::get('/politique-de-cookies', function () {
    return view('cookies');
})->name('cookies');
Route::get('/path/{id}/preview', [PathController::class, 'showAnonymous'])->name('path.show');

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/statistics', [StatisticsController::class, 'index']);
    Route::get('/statistics/{path}', [StatisticsController::class, 'show'])->name('statistics-for-path');
    Route::post('/attachments', [AttachmentController::class, 'store'])->name('attachments.store');

    /**
     * Path management
     */
    Route::get('/path/create', [PathController::class, 'create'])->name('path.create');
    Route::delete('/path/{path}', [PathController::class, 'destroy'])->name('path.destroy');
    Route::get('/path/{path}/edit', [PathController::class, 'edit'])->name('path.edit');
    Route::get('/path/{path}/edit-timeline', [PathController::class, 'editTimeline'])->name('path.edit-timeline');
    Route::put('/path/{path}', [PathController::class, 'update'])->name('path.update');
    Route::put('/path/{path}/update-timeline', [PathController::class, 'updateTimeline'])->name('path.update-timeline');
    Route::get('/path/{path}/preview-timeline', [PathController::class, 'previewTimeline'])->name('path.preview-timeline');
    Route::put('/path/{path}/steps-order', [PathController::class, 'updateStepsOrder']);
    Route::put('/path/{path}/toggle', [PathController::class, 'togglePublicStatus'])->name('path.toggle');
    Route::get('/admin/path/{id}', [PathController::class, 'showAdmin'])->name('path.show.admin');
    Route::get('/path/{path}/download', [PathController::class, 'download'])->name('path.download');

    /**
     * Step management
     */
    Route::get('/path/{id}/step/create', [StepController::class, 'create'])->name('step.create');
    Route::delete('/step/{step}', [StepController::class, 'destroy'])->name('step.destroy');
    Route::get('/step/{step}/edit', [StepController::class, 'edit'])->name('step.edit');
    Route::put('/step/{step}', [StepController::class, 'update'])->name('step.update');
    Route::get('/step/{step}/preview-itinerary', [StepController::class, 'previewItinerary'])->name('step.preview.itinerary');
    Route::get('/step/{step}/preview-site', [StepController::class, 'previewSite'])->name('step.preview.site');
    Route::get('/step/{step}/download', [StepController::class, 'download'])->name('step.download');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('/user/change-name', function () {
        return view('user.change-name');
    })->name('user.edit-name');
    Route::post('/user/change-name', [UserController::class, 'updateName'])->name('user.update-name');
    Route::delete('/user/{user}', [UserController::class, 'destroy'])->name('user.destroy');
    Route::get('/user/change-password', function () {
        return view('user.change-password');
    })->name('user.edit-password');
    Route::post('/user/change-password', [UserController::class, 'updatePassword'])->name('user.update-password');

    Route::post('/path/{path}/start', [PathController::class, 'start'])->name('path.start');
    Route::post('/path/{path}/restart', [PathController::class, 'restart'])->name('path.restart');
    Route::post('/path/{path}/end', [PathController::class, 'end'])->name('path.end');
    Route::post('/path/{path}/complete-timeline', [PathController::class, 'completeTimeline'])->name('path.complete-timeline');
    Route::get('/path/{path}', [PathController::class, 'showUser'])->name('path.show.user');

    Route::post('/step/{step}/ask-for-hint', [StepController::class, 'askForHint'])->name('step.ask-for-hint');
    Route::post('/step/{step}/complete', [StepController::class, 'complete'])->name('step.complete');
    Route::post('/step/{step}/answer-question', [StepController::class, 'answerBonusQuestion'])->name('step.answer-bonus-question');
    Route::get('/step/{step}', [StepController::class, 'show'])->name('step.show');
});

Route::middleware(['qrcode'])->group(function () {
    Route::get('/step/{step}/complete/{code}', [StepController::class, 'complete'])->name('step.complete.by.url');
});

Route::get('/error', function () {
    throw new Exception('My first GlitchTip error!');
});

require __DIR__.'/auth.php';
