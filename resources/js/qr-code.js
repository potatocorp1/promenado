import {Html5Qrcode} from "html5-qrcode"

const html5QrCode = new Html5Qrcode("reader");
const qrCodeSuccessCallback = (decodedText, decodedResult) => {
    window.location = decodedText;
};
const config = { qrbox: { width: 250, height: 250 } };

// back camera on default
html5QrCode.start({ facingMode: "environment" }, config, qrCodeSuccessCallback);