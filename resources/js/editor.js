const { default: axios } = window.axios

import Quill from 'quill'
import 'quill/dist/quill.snow.css'

const textAreaIds = [
    'itinerary',
    'hint',
    'on_site',
    'description',
]
textAreaIds.forEach(function (textArea) {
    if (document.getElementById(textArea) !== null) {
        activateEditor(textArea)
    }
})

document.querySelectorAll('div.cke_textarea_inline').forEach(function (div) {
    div.classList.add('form-control')
})

// Populate hidden form on submit
var form = document.querySelector('form');
form.onsubmit = function() {
    textAreaIds.forEach(function (textArea) {
        if (document.getElementById(textArea) !== null) {
            var input = document.querySelector('input[name='+textArea+']')
            input.value = document.querySelector('#' + textArea + '> div.ql-editor').innerHTML
        }
    })
}

function activateEditor(textareaId) {
    new Quill('#'+textareaId, {
        modules: {
          toolbar: [
            ['bold', 'italic'],
            ['blockquote', 'image'],
            [{ list: 'ordered' }, { list: 'bullet' }]
          ]
        },
        theme: 'snow'
    });
}