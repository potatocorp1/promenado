<x-app-layout>

    <x-slot name="header">
        Inscription
    </x-slot>

    <!-- Validation Errors -->
    <x-auth-validation-errors :errors="$errors" style="max-width: 500px" />

    <form method="POST" action="{{ route('register') }}" style="max-width: 500px" class="mx-auto">
        @csrf

        <!-- Name -->
        <div>
            <x-label for="name" value="Nom" class="form-label" />
            <x-input id="name" class="form-control" type="text" name="name" :value="old('name')" required autofocus />
        </div>

        <!-- Email Address -->
        <div class="mt-4">
            <x-label for="email" value="Adresse mail" class="form-label" />
            <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" required />
        </div>

        <!-- Password -->
        <div class="mt-4">
            <x-label for="password" value="Mot de passe" class="form-label" />
            <x-input id="password" class="form-control" 
                            type="password"
                            name="password"
                            required autocomplete="new-password" />
        </div>

        <!-- Confirm Password -->
        <div class="mt-4">
            <x-label for="password_confirmation" value="Confirmez le mot de passe" class="form-label" />
            <x-input id="password_confirmation" class="form-control" 
                            type="password"
                            name="password_confirmation" required />
        </div>

        <div>
            <x-button style="display:block" class="mx-auto my-4">
                Je m'inscris
            </x-button>

            <a style="text-align: center; display: block;" href="{{ route('login') }}">
                J'ai déjà un compte
            </a>
        </div>
    </form>
</x-app-layout>
