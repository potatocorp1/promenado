<x-app-layout>
    <x-slot name="header">
        Mot de passe oublié
    </x-slot>

    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <!-- Validation Errors -->
    <x-auth-validation-errors :errors="$errors" />

    <form method="POST" action="{{ route('password.email') }}">
        @csrf

        <!-- Email Address -->
        <div>
            <x-label for="email" value="Indiquez votre adresse mail" class="form-label" />
            <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" required autofocus />
        </div>

        <div class="mx-auto mt-4">
            <x-button>
                Envoyer un mail de récupération
            </x-button>
        </div>
    </form>
</x-app-layout>
