<x-app-layout>
    <x-slot name="header">
        Réinitialisation de mot de passe
    </x-slot>

    <!-- Validation Errors -->
    <x-auth-validation-errors :errors="$errors" />

    <form method="POST" action="{{ route('password.update') }}">
        @csrf

        <!-- Password Reset Token -->
        <input type="hidden" name="token" value="{{ $request->route('token') }}">

        <!-- Email Address -->
        <div class="mt-4">
            <x-label for="email" value="Adresse mail" class="form-label" />
            <x-input id="email" class="form-control" type="email" name="email" :value="old('email', $request->email)" required autofocus />
        </div>

        <!-- Password -->
        <div class="mt-4">
            <x-label for="password" value="Mot de passe" class="form-label" />
            <x-input id="password" class="form-control" type="password" name="password" required />
        </div>

        <!-- Confirm Password -->
        <div class="mt-4">
            <x-label for="password_confirmation" value="Confirmez le mot de passe" class="form-label" />
            <x-input id="password_confirmation" class="form-control" type="password" name="password_confirmation" required />
        </div>

        <div class="flex items-center justify-end mt-4">
            <x-button>Réinitialiser le mot de passe</x-button>
        </div>
    </form>
</x-app-layout>
