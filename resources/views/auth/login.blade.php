<x-app-layout>

    <x-slot name="header">
        Connexion
    </x-slot>

    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" style="max-width: 500px" />

    <!-- Validation Errors -->
    <x-auth-validation-errors :errors="$errors" style="max-width: 500px" />

    <form method="POST" action="{{ route('login') }}" style="max-width: 500px" class="mx-auto">
        @csrf

        <!-- Email Address -->
        <div>
            <x-label for="email" value="Email" class="form-label" />
            <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" required autofocus />
        </div>

        <!-- Password -->
        <div class="mt-4">
            <x-label for="password" value="Mot de passe" class="form-label" />
            <x-input id="password" class="form-control"
                            type="password"
                            name="password"
                            required autocomplete="current-password" />
        </div>

        <!-- Remember Me -->
        <div class="form-check mt-4">
            <input id="remember_me" type="checkbox" class="form-check-input" name="remember">
            <label for="remember_me" class="form-check-label">
                Se souvenir de moi
            </label>
        </div>

        <div>
            <x-button style="display:block" class="mx-auto my-4">
                Connexion
            </x-button>

            @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}" style="text-align: center; display: block;">
                    Mot de passe oublié ?
                </a>
            @endif

            <a href="{{ route('register') }}" style="text-align: center; display: block;">Pas encore de compte ?</a>

        </div>
    </form>

</x-app-layout>
