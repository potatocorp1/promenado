<x-app-layout>
    <x-slot name="header">
        Mentions légales
    </x-slot>

    <h2>Mentions légales du site Promenatour</h2>
    <p>
        Le site <a href="https://promenatour.fr">promenatour.fr</a> est édité par la mairie de Saint-Sauvant :<br><br>
        Mairie de Saint-Sauvant<br>
        1 Place de la Mairie<br>
        86600 Saint-Sauvant<br>
        Tél. : 0549597020<br>
        E-mail : accueil@saint-sauvant.fr<br>
        <br>
        Le directeur de publication est le maire de Saint-Sauvant, Monsieur Christophe Chappet
    </p>

    <h2>Hébergement</h2>
    <p>
        Gandi SAS<br>
        63-65 boulevard Masséna<br>
        75013 Paris FRANCE<br>
        Téléphone : 01 70 37 76 61
    </p>

    <h2>Création graphique, intégration et développement</h2>
    <p>
        EI Noémie Sachot<br>
        SIRET: 90018620600018<br>
        Mail : contact@noemie-sachot.fr<br>
        Site web : <a href="https://noemie-sachot.fr">noemie-sachot.fr</a>
    </p>

    <h2>Crédits photos</h2>
    <p>
        Photos et logo par Thibaud Guillard
    </p>

    <h2>Utilisation des données personnelles collectées</h2>

    <h3>Compte utilisateur</h3>
    <p>
        Des données à caractère personnel sont collectées sur le site ; il s’agit pour l’utilisateur de :
        <ul>
            <li>son pseudo,</li>
            <li>son adresse email</li>
        </ul>
        Ces données ne sont pas publiques et ne pourront être consultés que par les administrateurs du site.<br>
        <br>
        Ces données permettent à chaque utilisateur :
        <ul>
            <li>de se connecter à son compte,</li>
            <li>de lancer le parcours touristique Promenatour,</li>
            <li>de mémoriser et de suivre sa progression sur le parcours</li>
        </ul>
        Collectées conformément à la loi informatique et libertés de 2018 et au Règlement Européen pour la Protection des Données (RGPD), lesdites données ne sont jamais communiquées à un tiers.<br>
        <br>
        Chaque utilisateur dispose personnellement d’un droit d’accès, de rectification, d'effacement, de limitation, de portabilité, d'opposition sur ses propres données personnelles, pouvant être exercé à l’adresse : promenatour@gmail.com<br>
    </p>

    <h3>Cookies</h3>
    Voir page <a href="{{ route('cookies') }}">Politique de cookies</a>.<br>

</x-app-layout>
