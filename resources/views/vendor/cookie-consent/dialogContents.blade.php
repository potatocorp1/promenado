<div class="js-cookie-consent cookie-consent bg-secondary fixed-bottom d-flex justify-content-center align-items-center py-3">
    <p class="mb-0 px-2 cookie-consent__message">{!! trans('cookie-consent::texts.message') !!}</p>
    <button type="button" class="js-cookie-consent-agree cookie-consent__agree btn btn-primary mx-2">{{ trans('cookie-consent::texts.agree') }}</button>
    <a href="{{ route('cookies') }}">En savoir plus</a>
</div>
