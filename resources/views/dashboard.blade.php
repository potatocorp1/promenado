<x-app-layout>
    <x-slot name="header">
        Mon profil
    </x-slot>

    <div class="alert alert-success" role="alert">
        Bonjour {{ Auth::user()->name }} !
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="mt-2">
        @csrf
        <button class="btn btn-secondary">Se déconnecter</button>
    </form>

    <a href="{{ route('user.edit-name') }}" class="d-block">Changer mon nom</a>
    <a href="{{ route('user.edit-password') }}" class="d-block">Changer mon mot de passe</a>

    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteUserModal">Supprimer mon compte</button>

    <!-- Modal -->
    <div class="modal fade" id="deleteUserModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Voulez-vous vraiment supprimer votre compte ?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
            </div>
            <div class="modal-body">
                Toute votre progression sera perdue et cette action est définitive.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                <form action="{{ route('user.destroy', Auth::user()) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Supprimer mon compte</button>
                </form>
            </div>
            </div>
        </div>
    </div>
</x-app-layout>
