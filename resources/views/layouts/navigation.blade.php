<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">


    <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow-lg">
        <div class="container-fluid">

            <a class="navbar-brand" href="/">
                <img src="{{ asset('img/logo.png') }}" alt="" width="30" height="30" class="d-inline-block align-text-top me-2">
                {{ config('app.name') }}
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarColor02">
                <ul class="navbar-nav me-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/about">A propos</a>
                    </li>

                    @if (Auth::user() && Auth::user()->isAdmin())
                    <li class="nav-item">
                        <a class="nav-link" href="/statistics">Statistiques</a>
                    </li>
                    @endif
                </ul>


                <div class="d-flex">
                    @if (Route::has('login'))
                        <ul class="navbar-nav me-auto hidden">
                            @auth
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('dashboard') }}">{{ Auth::user()->name }}</a>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a href="{{ route('login') }}" class="nav-link">Connexion</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a href="{{ route('register') }}" class="nav-link">Inscription</a>
                                    </li>
                                @endif
                            @endauth
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </nav>

</nav>
