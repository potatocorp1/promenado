<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>
        <link rel="icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        @vite(['resources/css/app.css', 'resources/js/app.js'])

    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header>
                <h1 class="text-center fs-1 mt-4" style="font-variant: small-caps; margin: auto; letter-spacing: 2px;">
                    {{ $header }}
                </h1>
            </header>

            <!-- Page Content -->
            <main>
                <div class="container mt-4">
                    @if (null !== session('csrfTokenError'))
                    <div class="alert alert-warning" role="alert">
                        {{ session('csrfTokenError') }}
                    </div>
                    @endif

                    {{ $slot }}
                </div>
            </main>

            <!-- Page Footer -->
            @include('layouts.footer')
            @include('cookie-consent::index')
        </div>

        @yield('js')
    </body>
</html>
