<nav x-data="{ open: false }" class="bg-white border-b border-gray-100 mt-4">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow-lg">
        <div class="container">

            <ul class="navbar-nav m-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('legals') }}">Mentions légales</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('cookies') }}">Politique de cookies</a>
                </li>
            </div>
        </div>
    </nav>

</nav>
