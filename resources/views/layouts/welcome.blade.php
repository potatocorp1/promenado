<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>
        <link rel="icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <section class="py-5 text-center container-fluid bg-light">
                <div class="row py-lg-5">
                    <div class="col-lg-6 col-md-8 mx-auto">
                    <h1>Bienvenue sur Promenatour</h1>
                    <p class="lead text-muted">
                        Les circuits présents et à venir sont en partie créés par les enfant de l’école de Saint-Sauvant<br>
                        Venez découvrir les histoires originales de chaque site de Saint-Sauvant à travers notre 1er parcours !
                    </p>
                    <p>
                        <a href="{{ $ctaLink }}" class="btn btn-primary my-2">Je découvre !</a>
                    </p>
                    </div>
                </div>
            </section>

            <!-- Page Content -->
            <main>
                <div class="container mt-4">
                    {{ $slot }}
                </div>
            </main>

            <!-- Page Footer -->
            @include('layouts.footer')
            @include('cookie-consent::index')
        </div>

        @yield('js')
    </body>
</html>
