<x-app-layout>
    <x-slot name="header">
        Politique de cookies
    </x-slot>

    <p>
        Cette politique de cookies a été mise à jour pour la dernière fois le 31 janvier 2023 et s’applique aux citoyens et aux résidents permanents légaux de l’Espace Économique Européen et de la Suisse.
    </p>

    <h2>1. Introduction</h2>
    <p>
        Notre site web, promenatour.fr (ci-après : « le site web ») utilise des cookies et autres technologies liées (par simplification, toutes ces technologies sont désignées par le terme « cookies »). Des cookies sont également placés par des tierces parties que nous avons engagées. Dans le document ci-dessous, nous vous informons de l’utilisation des cookies sur notre site web.
    </p>

    <h2>2. Que sont les cookies ?</h2>
    <p>
        Un cookie est un petit fichier simple envoyé avec les pages de ce site web et stocké par votre navigateur sur le disque dur de votre ordinateur ou d’un autre appareil. Les informations qui y sont stockées peuvent être renvoyées à nos serveurs ou aux serveurs des tierces parties concernées lors d’une visite ultérieure.
    </p>
    <p>
        Certains cookies assurent le fonctionnement correct de certaines parties du site web et la prise en compte de vos préférences en tant qu’utilisateur. En plaçant des cookies fonctionnels, nous vous facilitons la visite de notre site web. Ainsi, vous n’avez pas besoin de saisir à plusieurs reprises les mêmes informations lors de la visite de notre site web et, par exemple, votre progression peut être sauvegardée pendant la durée de votre parcours touristique. Nous pouvons placer ces cookies sans votre consentement.
    </p>

    <h2>3. Liste des cookies placés</h2>
    <p>
        Notre site web n'utilise strictement que des cookies nécessaires à son bon fonctionnement. Aucun cookies marketing ou statistique ne sont utilisés.
    </p>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Expiration</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>laravel_cookie_consent</td>
                <td>365 jours</td>
            <tr>
            <tr>
                <td>promenatour_session</td>
                <td>1 jour</td>
            <tr>
            <tr>
                <td>XSRF-TOKEN</td>
                <td>1 jour</td>
            <tr>
        </tbody>
    </table>

</x-app-layout>
