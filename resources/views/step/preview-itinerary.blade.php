<x-app-layout>
    <x-slot name="header">
    Aperçu itinéraire de "{{ $step->name }}"
    </x-slot>

    <a href="{{ route('path.show.admin', $step->path_id) }}" class="btn btn-primary d-block mb-4" style="width: fit-content;">
        <i class="bi bi-arrow-left" style="color: white"></i> Retour au parcours
    </a>

    @if ($step->cover !== null)
    <div class="div-cover">
        <x-img-cover src="{{ asset('storage/'.$step->cover) }}" />
    </div>
    @endif

    <span class="step-custom-text">
        {!! $step->itinerary !!}
    </span>

    <div class="alert alert-light step-custom-text" role="alert">
        <strong>Indice qui apparaitra à la demande de l'utilisateur :</strong>
        {!! $step->hint !!}
    </div>

</x-app-layout>