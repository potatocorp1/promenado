<x-app-layout>
    <x-slot name="header">
    Aperçu sur place de "{{ $step->name }}"
    </x-slot>

    <a href="{{ route('path.show.admin', $step->path_id) }}" class="btn btn-primary d-block mb-4" style="width: fit-content;">
        <i class="bi bi-arrow-left" style="color: white"></i> Retour au parcours
    </a>

    @if ($step->cover !== null)
    <div class="div-cover">
        <x-img-cover src="{{ asset('storage/'.$step->cover) }}" />
    </div>
    @endif

    <span class="step-custom-text">
        {!! $step->on_site !!}
    </span>

    @if ($step->question)
        <div class="alert alert-light step-custom-text" role="alert">

            {{ $step->question }}

            @foreach ($step->answer_options as $index => $option)
            <div class="form-check">
                <input class="form-check-input" type="radio" name="answer_index" id="answer_index[{{ $index }}]" value="{{ $index }}">
                <label class="form-check-label" for="answer_index[{{ $index }}]">
                    {{ $option }}
                </label>
            </div>
            @endforeach

        </div>
    @endif

</x-app-layout>