<x-app-layout>
    <x-slot name="header">
    Etape : {{ $step->name }}
    </x-slot>

    <div class="d-flex align-items-center mb-4">
        <div class="progress flex-grow-1 me-2">
            <div class="progress-bar progress-bar-striped bg-warning" 
                role="progressbar" 
                aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100"
                style="width: {{ $progress }}%">
            </div>
        </div>
    </div>

    @if ($step->cover !== null)
    <div class="div-cover">
        <x-img-cover src="{{ asset('storage/'.$step->cover) }}" />
    </div>
    @endif

    <span class="step-custom-text">
        {!! $step->itinerary !!}
    </span>


    @if ($step->hintCanBeRequestedBy(Auth::user()))
    <div class="alert alert-light" role="alert">
        Bloqué ?

        <form action="{{ route('step.ask-for-hint', $step->id) }}" method="POST" class="d-inline">
            @csrf
            <button class="btn btn-link" type="submit" style="text-transform: none;">Cliquez ici pour obtenir un indice</button>
        </form>
    </div>
    @endif

    @if ($step->hintIsDisplayedFor(Auth::user()))
    <div class="alert alert-light step-custom-text" role="alert">
        {!! $step->hint !!}
    </div>
    @endif


    @if (null !== session('status_warning'))
    <div class="alert alert-warning" role="alert">
        {{ session('status_warning') }}
    </div>
    @endif

    <div id="reader" width="600px"></div>

    <form action="{{ route('step.complete', $step->id) }}" method="POST">
        @csrf
        <div class="row g-3 align-items-center my-4">
            <div class="col-auto">
                <label for="code" class="col-form-label">Ou entrez manuellement le code sous le qr-code</label>
            </div>
            <div class="col-auto">
                <input type="text" id="code" name="code" class="form-control">
            </div>
            <div class="col-auto">
                <button class="btn btn-primary" type="submit">Valider</button>
            </div>
        </div>
    </form>


    @section('js')
    @vite(['resources/js/qr-code.js',])
    @endsection

</x-app-layout>
