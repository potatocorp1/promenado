<x-app-layout>
    <x-slot name="header">
    Etape : {{ $step->name }}
    </x-slot>

    <div class="d-flex align-items-center mb-4">
        <div class="progress flex-grow-1 me-2">
            <div class="progress-bar progress-bar-striped bg-warning" 
                role="progressbar" 
                aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100"
                style="width: {{ $progress }}%">
            </div>
        </div>
    </div>

    @if ($step->cover !== null)
    <div class="div-cover">
        <x-img-cover src="{{ asset('storage/'.$step->cover) }}" />
    </div>
    @endif

    <span class="step-custom-text">
        {!! $step->on_site !!}
    </span>

    @if ($step->question)

        @if (null !== session('status_warning'))
        <div class="alert alert-warning step-custom-text" role="alert">
            {{ session('status_warning') }}
        </div>
        @endif

        <div class="alert alert-light" role="alert">
        {{ $step->question }}

        @if ($step->questionIsAlreadyAnsweredBy(Auth::user()))

            @foreach ($step->answer_options as $index => $option)
            <div class="form-check">
                <input class="form-check-input" type="radio" value="{{ $index }}" disabled name="flexRadioDefault" id="flexRadioDefault1"
                    @if ($step->answer_index == $index) 
                        checked
                    @endif
                >
                <label for="flexRadioDefault1" @class([
                    'form-check-label',
                    'text-success' => $step->answer_index == $index
                ])>
                    {{ $option }}
                </label>
            </div>
            @endforeach

        @else
            <form action="{{ route('step.answer-bonus-question', $step->id) }}" method="POST">
                @csrf

                @foreach ($step->answer_options as $index => $option)
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="answer_index" id="answer_index[{{ $index }}]" value="{{ $index }}">
                    <label class="form-check-label" for="answer_index[{{ $index }}]">
                        {{ $option }}
                    </label>
                </div>
                @endforeach

                <div>
                    <button class="btn btn-primary mt-3" type="submit">Valider</button>
                </div>
            </form>

        @endif

        </div>
    @endif


    @if ($progress == 100)
        <form action="{{ route('path.end', $step->path->id) }}" method="POST">
            @csrf
            @method('POST')
            <button class="btn btn-primary">Terminer le parcours</button>
        </form>
    @else
        <a href="{{ route('step.show', $step->path->getNextStep(Auth::user())) }}" class="btn btn-primary">Passer à l'étape suivante</a>
    @endif

</x-app-layout>
