<x-app-layout>
    <x-slot name="header">
    {{ $title }}
    </x-slot>

    @if ($errors->any())
        <div class="alert alert-warning" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ $action }}" class="mx-auto row" enctype="multipart/form-data">
        @csrf
        @method($method)
        <input type="hidden" id="path_id" name="path_id" value="{{ $step->path_id }}">

        <!-- Name -->
        <div class="row mb-3">
            <x-label for="name" value="Nom" class="col-sm-2 col-form-label text-end required" />
            <div class="col-sm-10">
                <x-input id="name" class="form-control" type="text" name="name" value="{{ old('name', $step->name) }}" required autofocus />
            </div>
        </div>

        <!-- Cover -->
        <div class="row mb-3">
            <x-label for="cover" value="Image" class="col-sm-2 col-form-label text-end" />
            <div class="col-sm-10">
                <x-input id="cover" class="form-control mb-2" type="file" name="cover" value="{{ old('cover', $step->cover) }}" autofocus />
                <x-img-cover src="{{ $step->cover == null ? asset('img/placeholder.jpeg') :  asset('storage/'.$step->cover) }}" id="cover-preview" height=500 />
            </div>
        </div>

        <!-- Code -->
        <div class="row mb-3">
            <x-label for="code" value="Code" class="col-sm-2 col-form-label text-end required" />
            <div class="col-sm-10">
                <x-input id="code" class="form-control" type="text" name="code" value="{{ old('code', $step->code) }}" required autofocus />
            </div>
        </div>

        <!-- Itinerary text -->
        <div class="row mb-3 has-rich-text">
            <x-label for="itinerary" value="Texte d'itinéraire" class="col-sm-2 col-form-label text-end required" />
            <input name="itinerary" type="hidden">
            <div class="col-sm-10">
                <div id="itinerary" data-id="{{ $step->id }}" data-type="{{ get_class($step) }}" data-url="{{ route('attachments.store') }}">
                    {!! old('itinerary', $step->itinerary) !!}
                </div>
            </div>
        </div>

        <!-- Optional hint -->
        <div class="row mb-3 has-rich-text">
            <x-label for="hint" value="Indice" class="col-sm-2 col-form-label text-end" />
            <input name="hint" type="hidden">
            <div class="col-sm-10">
                <div id="hint" data-id="{{ $step->id }}" data-type="{{ get_class($step) }}" data-url="{{ route('attachments.store') }}">
                    {!! old('hint', $step->hint) !!}
                </div>
            </div>
        </div>

        <!-- On-site text -->
        <div class="row mb-3 has-rich-text">
            <x-label for="on_site" value="Texte sur place" class="col-sm-2 col-form-label text-end required" />
            <input name="on_site" type="hidden">
            <div class="col-sm-10">
                <div id="on_site" data-id="{{ $step->id }}" data-type="{{ get_class($step) }}" data-url="{{ route('attachments.store') }}">
                    {!! old('on_site', $step->on_site) !!}
                </div>
            </div>
        </div>

        <!-- Optional bonus question -->
        <div class="row mb-3">
            <x-label for="question" value="Question bonus" class="col-sm-2 col-form-label text-end" />
            <div class="col-sm-10">
                <x-input id="question" class="form-control" type="text" name="question" value="{{ old('question', $step->question) }}" autofocus />
            </div>
        </div>

        <!-- Answer options -->
        <div class="row mb-3">
            <x-label for="question" value="Réponses possibles" class="col-sm-2 col-form-label text-end" />
            <div class="col-sm-10">
                @foreach ($step->answer_options as $index => $option)
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="answer_index" id="answer_index[{{ $index }}]" value="{{ $index }}" style="margin-top: 10px" @if($step->answer_index == $index) checked @endif>
                    <label class="form-check-label" for="answer_index[{{ $index }}]">
                        <x-input id="answer_options[{{ $index }}]" class="form-control" type="text" name="answer_options[{{ $index }}]" value="{{ old('answer_options['.$index.']', $option) }}" autofocus />
                    </label>
                </div>
                @endforeach
            </div>
        </div>
 
        <x-button class="col-sm-4 mb-5">
            {{ $buttonLabel }}
        </x-button>
    </form>

    @section('js')
    @vite(['resources/js/editor.js', 'resources/js/cover-preview.js'])
    @endsection

</x-app-layout>
