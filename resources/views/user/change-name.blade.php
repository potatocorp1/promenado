<x-app-layout>

    <x-slot name="header">
        Votre nom utilisateur
    </x-slot>

    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" style="max-width: 500px" />

    <!-- Validation Errors -->
    <x-auth-validation-errors :errors="$errors" style="max-width: 500px" />

    <form method="POST" action="{{ route('user.update-name') }}" style="max-width: 500px" class="mx-auto">
        @csrf

        <!-- Name -->
        <div>
            <x-label for="name" value="Nouveau nom" class="form-label" />
            <x-input id="name" class="form-control" type="text" name="name" :value="old('name')" required autofocus />
        </div>

        <x-button style="display:block" class="mx-auto my-4">
            Enregistrer
        </x-button>
    </form>

</x-app-layout>
