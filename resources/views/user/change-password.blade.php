<x-app-layout>

    <x-slot name="header">
        Nouveau mot de passe
    </x-slot>

    <!-- Session Status -->
    @if (null !== session('status_warning'))
    <div class="alert alert-warning m-auto" role="alert" style="max-width: 500px">
        {{ session('status_warning') }}
    </div>
    @endif

    <!-- Validation Errors -->
    <x-auth-validation-errors :errors="$errors" style="max-width: 500px" />

    <form method="POST" action="{{ route('user.update-password') }}" style="max-width: 500px" class="mx-auto">
        @csrf

        <!-- Old password -->
        <div class="mt-4">
            <x-label for="oldPassword" value="Mot de passe actuel" class="form-label" />
            <x-input id="oldPassword" class="form-control" 
                            type="password"
                            name="oldPassword"
                            required />
        </div>

        <!-- Password -->
        <div class="mt-4">
            <x-label for="password" value="Nouveau mot de passe" class="form-label" />
            <x-input id="password" class="form-control" 
                            type="password"
                            name="password"
                            required />
        </div>

        <!-- Confirm Password -->
        <div class="mt-4">
            <x-label for="password_confirmation" value="Confirmez le mot de passe" class="form-label" />
            <x-input id="password_confirmation" class="form-control" 
                            type="password"
                            name="password_confirmation" required />
        </div>

        <x-button style="display:block" class="mx-auto my-4">
            Enregistrer
        </x-button>
    </form>

</x-app-layout>
