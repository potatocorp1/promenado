<x-app-layout>
    <x-slot name="header">
        A propos
    </x-slot>

    <h2>Qu’est ce que Promenatour ?</h2>
    <div class="div-description-with-image">
        <img src="{{ asset('img/full_logo.png') }}" style="width: 15rem; height: 15rem;" />
        <span>
            <p>
                Promenatour est un circuit touristique créé par Thibaud GUILLARD, étudiant en BTS Tourisme à Isaac de l’étoile à Poitiers.<br><br>
                Le circuit à été créé pour 2 raisons :
                <ul>
                    <li>Remettre en éveil le patrimoine et l’histoire du bourg de Saint-Sauvant à travers des étapes</li>
                    <li>Mettre en place un projet étudiant</li>
                </ul>
            </p>
        </span>
    </div>

    <h2>Comment fonctionne Promenatour ?</h2>
    <p>
        Le circuit fonctionne par étape, le départ se fait à la Mairie de Saint-Sauvant.<br>
        Sur votre Smartphone ou tablette vous verrez une ancienne image suivi d’un périmètre du lieu.<br>
        Dès que vous avez trouvé le site, vous chercherez sur place un QR code qui vous permettra de faire la suite.<br>
        Une fois le scan effectué une brève histoire sur le lieu avec sa photo actuelle et un « mini Quizz » va se lancer.<br>
        Vous aurez alors une question sous forme de QCM (Question à Choix Multiple) et vous aurez 3 réponses avec 3 essais.<br>
        Une fois la question répondue, des points vous seront attribués, puis l'application lancera une prochaine étape du circuit aléatoirement.<br>
        Le circuit se finit au retour à la mairie, sur l’histoire de celle-ci.<br>
        A la fin du circuit, une frise chronologique apparait : sur la gauche une image, sur la droite une date, et il faudra associer les images avec les bonnes dates.<br>
    </p>

    <h3>Contacter / voir les actions</h3>
    <a href="mailto:wickel.owl@gmail.com"><i class="bi bi-envelope-fill me-2" style="font-size: 4rem; color: #304727"></i></a>
    <a href="https://www.facebook.com/nosptitsvoyageurs/" target="_blank"><i class="bi bi-facebook me-2" style="font-size: 4rem; color: #304727"></i></a>
    <a href="https://www.instagram.com/promenatour_st_sauvant/" target="_blank"><i class="bi bi-instagram" style="font-size: 4rem; color: #304727"></i></a>

</x-app-layout>
