<x-app-layout>
    <x-slot name="header">
        Statistiques
    </x-slot>

    <div class="alert alert-secondary">
        Nombre total d'utilisateurs inscrits :
        <strong>{{ $nbUsers }}</strong>
    </div>

    <h3 class="mt-5">Récapitulatif des inscriptions</h3>
    <table class="table table-hover" id="statisticsRegisteredUsers">
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Janvier</th>
                <th scope="col">Février</th>
                <th scope="col">Mars</th>
                <th scope="col">Avril</th>
                <th scope="col">Mai</th>
                <th scope="col">Juin</th>
                <th scope="col">Juillet</th>
                <th scope="col">Aout</th>
                <th scope="col">Septembre</th>
                <th scope="col">Obtobre</th>
                <th scope="col">Novembre</th>
                <th scope="col">Décembre</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($nbUsersPerYearsAndMonths as $year => $nbUserPerMonth)
                <tr>
                    <th scope="row">{{ $year }}</th>
                    @foreach ($nbUserPerMonth as $nb)
                        <td>{{ $nb }}</td>
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>

    <h3 class="mt-5">Statistiques par parcours</h3>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">Parcours</th>
                <th scope="col">Commencé</th>
                <th scope="col">Terminé</th>
                <th scope="col">Indices</th>
                <th scope="col">Temps moyen (min)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($statPerPath as $path)
                <tr>
                    <th scope="row"><a href="{{ route('statistics-for-path', $path['id']) }}">{{ $path['name'] }}</a></th>
                    <td>{{ $path['started'] }}</td>
                    <td>{{ $path['ended'] }}</td>
                    <td>{{ $path['nbHints'] }}</td>
                    <td>{{ $path['averageTime'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

</x-app-layout>