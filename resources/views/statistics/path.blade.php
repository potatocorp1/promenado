<x-app-layout>
    <x-slot name="header">
        Statistiques pour le parcours {{ $path->name }}
    </x-slot>

    <a href="/statistics">Retour aux statistiques générales</a>

    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">Utilisateurs</th>
                <th scope="col">Commencé</th>
                <th scope="col">Terminé</th>
                <th scope="col">Indices</th>
                <th scope="col">Temps moyen (min)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($path->users as $user)
                <tr>
                    <th scope="row">{{ $user->name }}</a></th>
                    <td>
                        @if ($user->pivot->started_at)
                            {{ $user->pivot->started_at->format('d/m/Y') }}
                        @endif
                    </td>
                    <td>
                        @if ($user->pivot->completed_at)
                            {{ $user->pivot->completed_at->format('d/m/Y') }}
                        @endif
                    </td>
                    <td>{{ $nbHints[$user->id] }}</td>
                    <td>{{ $time[$user->id] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

</x-app-layout>