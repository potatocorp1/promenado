<x-app-layout>
    <x-slot name="header">
        Vous n'avez pas les autorisations nécessaires
    </x-slot>

    <a class="btn btn-primary" href="{{ url('/') }}" role="button">Retourner à l'accueil</a>
</x-app-layout>