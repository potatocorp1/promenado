<x-welcome-layout>

    <x-slot name="ctaLink">
    {{ $ctaLink }}
    </x-slot>

    @if (Auth::user() && Auth::user()->isAdmin())
    <a class="btn btn-primary d-block mx-auto mb-4" style="width: fit-content;" href="{{ url('/path/create') }}" role="button">Ajouter un parcours</a>
    @endif

    @if (count($paths) > 0)

        <div style="display: flex; flex-wrap: wrap; justify-content: center;">
            @foreach ($paths as $path)
                @if ($path->is_public == true || ( Auth::user() && Auth::user()->isAdmin()) )
                    <div class="card" style="margin: 1em; flex-basis: 25rem;">

                        @if ($path->cover == null)
                        <x-img-cover src="{{ asset('img/placeholder.jpeg') }}" class="card-img-top" />
                        @else
                        <x-img-cover src="{{ asset('storage/'.$path->cover) }}" class="card-img-top" id="cover-{{ $path->id }}" />
                        @endif

                        <div class="card-body">
                            <h5 class="card-title">
                                @if (Auth::user() && ($path->getStatusForUser(Auth::user()) == 'completed') )
                                    <i class="bi bi-check-circle-fill"></i>&nbsp;
                                @endif
                                @if ($path->is_public == false)
                                    <span class="badge bg-warning">Brouillon</span>
                                @endif
                                {{ $path->name }}
                            </h5>
                            <span class="card-text description-extract">
                                {!! $path->description !!}
                            </span>
                            <p class="card-text"><i class="bi bi-pin-fill"></i> {{ $path->start_place }}</p>

                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    @if (Auth::user() && ($path->getStatusForUser(Auth::user()) == 'started') )
                                        @if ($path->getNextStep(Auth::user()) !== null)
                                            <a href="{{ route('step.show', $path->getNextStep(Auth::user())) }}" class="btn btn-sm btn-primary">Continuer</a>
                                        @endif
                                    @elseif (Auth::user() && ($path->getStatusForUser(Auth::user()) == 'completed') )
                                        <a href="{{ route('path.show.user', $path->id) }}" class="btn btn-sm btn-secondary">Voir mes résultats</a>
                                    @else
                                        <a href="{{ route('path.show', $path->id) }}" class="btn btn-sm btn-secondary">Voir</a>
                                    @endif

                                    @if (Auth::user() && Auth::user()->isAdmin())
                                        <a href="{{ route('path.show.admin', $path->id) }}" class="btn btn-sm btn-secondary">Gérer</a>
                                    @endif

                                    @if (Auth::user() && ($path->getStatusForUser(Auth::user()) !== 'not_started') )
                                        <div class="btn-group" role="group">
                                            <button id="btnGroup{{ $path->id }}" type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                                <i class="bi bi-three-dots-vertical"></i>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="btnGroup{{ $path->id }}">
                                                <li>
                                                    <form action="{{ route('path.restart', $path->id) }}" method="POST">
                                                        @csrf
                                                        @method('POST')
                                                        <button class="btn btn-link text-start">Recommencer le parcours</button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                <small class="text-muted ">{{ $path->duration }}</small>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    @else
        <p class="mt-4">Aucun parcours pour le moment</p>
    @endif

</x-app-layout>