@props(['src', 'id' => 'cover-image'])

<img src="{{ $src ?? $slot }}"
    id="{{ $id }}"
    width="100%"
    style="object-fit: cover;" 

    {{ $attributes->merge(['class' => '']) }}

    {{ $attributes->merge([
        'type' => 'button',
        'height' => 225
    ]) }} />
