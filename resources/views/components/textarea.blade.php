@props(['value'])

<textarea {{ $attributes->merge(['class' => '']) }}>
    {{ $value ?? $slot }}
</textarea>
