@props(['status'])

@if ($status)
    <div class="alert alert-success mx-auto" role="alert" {{ $attributes }}>
        {{ $status }}
    </div>
@endif
