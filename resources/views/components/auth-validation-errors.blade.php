@props(['errors'])

@if ($errors->any())
    <div class="alert alert-warning mx-auto" role="alert" {{ $attributes }}>
        <div class="fw-bold">
            Une erreur est survenue
        </div>

        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
