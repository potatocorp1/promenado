<button {{ $attributes->merge([
        'type' => 'submit',
        'class' => 'btn btn-primary mx-auto mt-4 d-block']) 
    }}>
    {{ $slot }}
</button>
