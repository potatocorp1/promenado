<x-app-layout>
    <x-slot name="header">
    Parcours : {{ $path->name }}
    </x-slot>

    @if (null !== session('status_success'))
    <div class="alert alert-success" role="alert">
        {{ session('status_success') }}
    </div>
    @endif
    @if (null !== session('status_warning'))
    <div class="alert alert-warning" role="alert">
        {{ session('status_warning') }}
    </div>
    @endif

    <div class="div-description-with-image">

        <span>
            @if ($path->cover == null)
            <x-img-cover src="{{ asset('img/placeholder.jpeg') }}" class="card-img-top" height="300" />
            @else
            <x-img-cover src="{{ asset('storage/'.$path->cover) }}" class="card-img-top" height="300" />
            @endif

            <div>
            {!! $path->description !!}
            </div>
        </span>

        <span class="ps-3">
            <div class="list-group">
                <a href="{{ route('path.edit', $path->id) }}" class="list-group-item list-group-item-action d-flex align-items-center gap-2" aria-current="true">
                    <span class="d-inline-block bg-primary rounded-circle p-1"></span>
                    Modifier le parcours
                </a>
                <a href="{{ route('path.edit-timeline', $path->id) }}" class="list-group-item list-group-item-action d-flex align-items-center gap-2">
                    <span class="d-inline-block bg-primary rounded-circle p-1"></span>
                    Gérer la timeline
                </a>

                <a href="{{ route('path.preview-timeline', $path->id) }}" 
                    @class([
                        'list-group-item',
                        'list-group-item-action',
                        'd-flex',
                        'align-items-center',
                        'gap-2',
                        'disabled' => !$path->hasTimeline()
                    ])>
                    <span @class([
                        'd-inline-block',
                        'rounded-circle',
                        'p-1',
                        'bg-dark' => $path->hasTimeline(),
                        'bg-warning' => !$path->hasTimeline(),
                    ])></span>
                    Aperçu de la timeline
                </a>

                <form action="{{ route('path.toggle', $path->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    @if ($path->is_public == true)
                        <button class="list-group-item list-group-item-action d-flex align-items-center gap-2">
                            <span class="badge bg-success rounded-pill">Public</span>
                            Repasser en brouillon
                        </button>
                    @else
                        <button class="list-group-item list-group-item-action d-flex align-items-center gap-2">
                            <span class="badge bg-warning rounded-pill">Brouillon</span>
                            Publier
                        </button>
                    @endif
                </form>

                <a href="{{ route('path.download', $path->id) }}" class="list-group-item list-group-item-action d-flex align-items-center gap-2">
                    <span class="d-inline-block bg-dark rounded-circle p-1"></span>
                    Imprimer le qr-code
                </a>

                <a href="#" class="list-group-item list-group-item-action d-flex align-items-center gap-2" data-bs-toggle="modal" data-bs-target="#deletePathModal">
                    <span class="d-inline-block bg-danger rounded-circle p-1"></span>
                    Supprimer le parcours
                </a>

            </div>
        </span>
    </div>

    <div id="app">
        <steps-table :init-steps="{{ $path->steps }}" :path-id="{{ $path->id }}" />
    </div>

    @if (count($path->steps) == 0)
        <p>Aucune étape pour ce parcours</p>
    @endif

    <a class="btn btn-primary" href="{{ route('step.create', $path->id) }}" role="button">Ajouter une étape</a>

    <!-- Modal step deletion -->
    <div class="modal fade" id="deletePathModal" tabindex="-1" aria-labelledby="deletePathModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deletePathModalLabel">Voulez-vous vraiment supprimer ce parcours ?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
            </div>
            <div class="modal-body">
                Les étapes du parcours et la progression des joueurs seront supprimés. Cette action est définitive.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                <form action="{{ route('path.destroy', $path->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Supprimer le parcours</button>
                </form>
            </div>
            </div>
        </div>
    </div>

</x-app-layout>
