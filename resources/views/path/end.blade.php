<x-app-layout>
    <x-slot name="header">
        Fin : {{ $name }}
    </x-slot>

    <div class="alert alert-success" role="alert">
        Bravo pour avoir terminé ce parcours !
    </div>

    <table class="table">
        <thead>
            <tr>
            <th scope="col">Etape</th>
            <th scope="col">Points</th>
            <th scope="col">Indice</th>
            <th scope="col">Question bonus</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($results as $result)
                <tr>
                    <td>{{ $result['name'] }}</td>
                    <td>{{ $result['base'] }}</td>
                    <td>{{ $result['hint'] }}</td>
                    <td>{{ $result['bonus'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h2 class="text-center">Score : <span class="badge bg-success">{{ $score }}</span> points !</h2>

    <a href="{{ route('main') }}" class="btn btn-primary d-block mx-auto my-4" style="width: fit-content;">Revenir aux parcours</a>
 
    <button type="button" class="btn btn-secondary d-block mx-auto" data-bs-toggle="modal" data-bs-target="#restartPath">Ou recommencer ce parcours maintenant !</button>

    <!-- Modal -->
    <div class="modal fade" id="restartPath" tabindex="-1" aria-labelledby="restartPathModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="restartPathModalLabel">Voulez-vous vraiment recommencer ce parcours ?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
            </div>
            <div class="modal-body">
                Toute votre progression sur ce parcours sera effacée et vous pourrez le recommencer depuis la 1ère étape
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                <form action="{{ route('path.restart', $pathId) }}" method="POST">
                    @csrf
                    @method('POST')
                    <button class="btn btn-primary">Recommencer ce parcours</button>
                </form>
            </div>
            </div>
        </div>
    </div>

</x-app-layout>