<x-app-layout>
    <x-slot name="header">
    Parcours : {{ $path->name }}
    </x-slot>

    {!! $path->description !!}

    <div class="d-flex align-items-center">
        <div class="progress flex-grow-1 me-2">
            <div class="progress-bar progress-bar-striped bg-warning" 
                role="progressbar" 
                aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100"
                style="width: {{ $progress }}%">
            </div>
        </div>
        @if ($progress == 100)
            <form action="{{ route('path.end', $path->id) }}" method="POST">
                @csrf
                @method('POST')
                <button class="btn btn-primary">Terminer le parcours</button>
            </form>
        @endif
    </div>

    <div style="display: flex; flex-wrap: wrap; justify-content: center;">
        @foreach ($path->steps as $step)
            <div class="card" style="width: 18rem; margin: 1em; flex-basis: 20rem;">
                @if ($step->cover == null)
                <x-img-cover src="{{ asset('img/placeholder.jpeg') }}"  class="card-img-top" />
                @else
                <x-img-cover src="{{ asset('storage/'.$step->cover) }}"  class="card-img-top" />
                @endif

                <div class="card-body">
                    <h5 class="card-title">
                        @if ($step->isCompletedForUser(Auth::user()))
                            <i class="bi bi-check-circle-fill"></i>&nbsp;
                        @endif
                        {{ $step->name }}
                    </h5>

                    @if ($step->isCompletedForUser(Auth::user()))
                        <a href="{{ route('step.show', $step->id) }}" class="btn btn-secondary">Voir</a>
                    @else
                        <a href="{{ route('step.show', $step->id) }}" class="btn btn-primary">Itinéraire</a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>

</x-app-layout>
