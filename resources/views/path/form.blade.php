<x-app-layout>
    <x-slot name="header">
    {{ $title }}
    </x-slot>



    @if ($errors->any())
        <div class="border-2 border-red-500">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ $action }}" class="mx-auto row" enctype="multipart/form-data">
        @csrf
        @method($method)

        <!-- Name -->
        <div class="row mb-3">
            <x-label for="name" value="Nom" class="col-sm-2 col-form-label text-end required" />
            <div class="col-sm-10">
                <x-input id="name" class="form-control" type="text" name="name" value="{{ old('name', $path->name) }}" required autofocus />
            </div>
        </div>

        <!-- Cover -->
        <div class="row mb-3">
            <x-label for="cover" value="Image" class="col-sm-2 col-form-label text-end" />
            <div class="col-sm-10">
                <x-input id="cover" class="form-control mb-2" type="file" name="cover" value="{{ old('cover', $path->cover) }}" autofocus />
                <x-img-cover src="{{ $path->cover == null ? asset('img/placeholder.jpeg') :  asset('storage/'.$path->cover) }}" id="cover-preview" height=500 />
            </div>
        </div>

        <!-- Duration -->
        <div class="row mb-3">
            <x-label for="duration" value="Durée estimée" class="col-sm-2 col-form-label text-end required" />
            <div class="col-sm-10">
                <x-input id="duration" class="form-control" type="text" name="duration" value="{{ old('duration', $path->duration) }}" placeholder="1h 30m" required autofocus />
            </div>
        </div>

        <!-- Description -->
        <div class="row mb-3 has-rich-text">
            <x-label for="description" value="Description" class="col-sm-2 col-form-label text-end required" />
            <input name="description" type="hidden">
            <div class="col-sm-10">
                <div id="description" data-id="{{ $path->id }}" data-type="{{ get_class($path) }}" data-url="{{ route('attachments.store') }}">
                    {!! old('description', $path->description) !!}
                </div>
            </div>
        </div>

        <!-- Start place -->
        <div class="row mb-3">
            <x-label for="start_place" value="Adresse de départ" class="col-sm-2 col-form-label text-end required" />
            <div class="col-sm-10">
                <x-input id="start_place" class="form-control" type="text" name="start_place" value="{{ old('start_place', $path->start_place) }}" required autofocus />
            </div>
        </div>
 
        <x-button class="col-sm-4 mb-5">
            {{ $buttonLabel }}
        </x-button>
    </form>

@section('js')
@vite(['resources/js/editor.js', 'resources/js/cover-preview.js'])
@endsection

</x-app-layout>
