<x-app-layout>
    <x-slot name="header">
        Timeline : {{ $path->name }}
    </x-slot>

    <div id="app">
        <timeline
            :timeline="{{ json_encode($path->timeline) }}"
            :steps-raw="{{ $timelineSteps }}"
            :path-id="{{$path->id}}"
        />
    </div>

</x-app-layout>
