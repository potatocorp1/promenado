<x-app-layout>
    <x-slot name="header">
    Parcours : {{ $path->name }}
    </x-slot>


    <div class="div-description-with-image">

        @if ($path->cover == null)
        <x-img-cover src="{{ asset('img/placeholder.jpeg') }}" class="card-img-top" height="300" />
        @else
        <x-img-cover src="{{ asset('storage/'.$path->cover) }}" class="card-img-top" height="300" />
        @endif

        <span>
            <h2>Description du parcours</h2>
            {!! $path->description !!}

            <h2>Informations pratiques</h2>
            <ul class="list-unstyled">
                <li><i class="bi bi-pin-fill"></i> {{ $path->start_place }}</li>
                <li><i class="bi bi-clock-fill"></i> Durée estimée : {{ $path->duration }}</li>
            </ul>
        </span>
    </div>

    <div class="alert alert-light" role="alert">
        <h4 class="alert-heading">Conditions d'utilisation</h4>
        <p>Merci de respecter les habitants, les propriétés privées et de faire attention à la circulation sur la route.<br>
        Et amusez-vous bien !</p>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="acceptanceCheckbox">
            <label class="form-check-label" for="acceptanceCheckbox">
                J'accepte les conditions d'utilisation
            </label>
        </div>
    </div>

    <form action="{{ route('path.start', $path->id) }}" method="POST">
        @csrf
        @method('POST')
        <button class="btn btn-primary disabled" id="lauchPathButton">Je commence le parcours !</button>
    </form>

    @section('js')
    <script type="text/javascript">
        document.getElementById('acceptanceCheckbox').addEventListener("change", function(){
            const isChecked = this.checked
            document.getElementById('lauchPathButton').classList.toggle('disabled')
        });
    </script>
    @endsection

</x-app-layout>
