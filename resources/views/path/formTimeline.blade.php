<x-app-layout>
    <x-slot name="header">
    Timeline de {{ $path->name }}
    </x-slot>

    @if ($errors->any())
        <div class="alert alert-warning" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('path.update-timeline', $path->id) }}" class="mx-auto row">
        @csrf
        @method('PUT')

        <div style="display: flex; flex-wrap: wrap; justify-content: center;">
        @foreach ($path->steps as $step)
            <div class="card" style="margin: 1em; flex-basis: 20rem;">

                @if ($step->cover == null)
                <x-img-cover src="{{ asset('img/placeholder.jpeg') }}" class="card-img-top" />
                @else
                <x-img-cover src="{{ asset('storage/'.$step->cover) }}" class="card-img-top" id="cover-{{ $step->id }}" />
                @endif

                <div class="card-body">
                    <input
                        class="form-control d-block m-auto w-50"
                        placeholder="Année"
                        type="text"
                        id="year[{{ $step->id }}]"
                        name="year[{{ $step->id }}]"
                        min="1000"
                        max="2500"
                        value="{{ old('year.'.$step->id, 
                                    (is_array($path->timeline) 
                                    && is_array($path->timeline['years']) 
                                    && isset($path->timeline['years'][$step->id]) 
                                    ? $path->timeline['years'][$step->id] : '')
                                    ) }}"
                        autofocus
                    />
                </div>
            </div>
        @endforeach
        </div>
 
        <x-button class="col-sm-4 mb-5">Mettre à jour la timeline</x-button>
    </form>

</x-app-layout>
