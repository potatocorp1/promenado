<x-app-layout>
    <x-slot name="header">
        Aperçu timeline : {{ $path->name }}
    </x-slot>

    <div class="container position-relative">
        <div style="height:25px;"></div>
        @foreach ($path->steps as $step)
            @if(in_array($step->id, array_keys($path->timeline['years'])))

                <div class="row justify-content-evenly mb-5">
                    <div class="col col-lg-4 position-relative">
                        <img src="{{ asset('storage/'.$step->cover) }}" style="border-radius: 1.5rem; width: 100%" class="border border-secondary border-5">
                        <p class="fs-3 d-inline-block position-absolute bg-light p-2 bg-opacity-75" style="top: 50%; left: 50%; transform: translate(-50%, -50%);">{{ $path->timeline['years'][$step->id] }}</p>
                    </div>
                </div>

            @endif
        @endforeach

        <div class="row justify-content-evenly">
            <div class="col col-lg-4 bg-light d-flex justify-content-center">
                <div class="bg-secondary d-inline-block position-absolute" style="width:2rem; height: calc(100% + 50px); top: 0; z-index: -1;"></div>
            </div>
        </div>
    </div>
</x-app-layout>