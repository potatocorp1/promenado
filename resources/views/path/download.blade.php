<style type="text/css">
    @font-face {
        font-family: "Montserrat";
        src: url('https://fonts.googleapis.com/css?family=Montserrat');
    }
    html, p {
        text-align: center;
    }
    h1 h2 h3{
        margin: 0px;
    }
    .card{
        border: solid 1px #dfdfdf;
        border-radius: 5px;
        font-family: 'Montserrat', sans-serif;
    }
    .head{
        border-bottom: solid 1px #dfdfdf;
        background-color: #f7f7f7;
        padding: 20px;
        border-radius: 5px 5px 0 0;
    }
    .content{
        padding: 20px;
    }
    h2{
        color: #333333;
    }
    h3{
        color: #999999;
    }
    p{
        color: #4d4d4d;
        text-align: justify;
        text-justify: inter-word;
    }
    .qr-code{
        text-align: center;
    }
    .footer{
        border-top: solid 1px #dfdfdf;
        background-color: #f7f7f7;
        color: #9a9a9a;
        padding: 20px;
        border-radius: 0 0 5px 5px;
    }
</style>

<div class="card">
    <div class="head">
        <h1>{{ $path->name }}</h1>
    </div>
    <div class="content">
        <p class="qr-code">
            <img src="data:image/png;base64,{{ base64_encode(file_get_contents($path->qrcode())) }}">
        </p>
    </div>
    <div class="footer">
        <em>Promenatour</em>
    </div>
</div>
