## Local Install

- Récupérer le projet `git clone git@gitlab.com:potatocorp1/promenado.git`
- `cd promenado`
- Installer les dépendances `composer install` et `npm install`
- Initialiser le fichier d'environnement `cp .env.example .env`
- Générer la clé `php artisan key:generate`
- Initialiser la DB `touch database/database.sqlite` et mettre à jour le .env pour indiquer le chemin absolu du fichier sqlite
- Jouer les migrations `php artisan migrate`
- Initialiser l'accès public aux images uploadées `php artisan storage:link`
- 1ère compilation des assets `npm run dev`

## Seeders disponibles

- Jeu de données de test avec `php artisan db:seed`
- Créé 2 utilisateurs : 
  + admin@admin.fr / useruser
  + user@user.fr / useruser

## Resources

- [Conception on FigJam](https://www.figma.com/file/bDVZS04IL5blCYuXkKhIOa/Promenado?node-id=0%3A1)
- [Timeline](https://www.figma.com/file/NkZX0yq4bwWQl7NW0GqEwm/Untitled)
- [Main page design inspiration](https://getbootstrap.com/docs/5.1/examples/album/)
- [Color palette](https://coolors.co/304727-ffcc00-fcfafa-0d3b66-f95738)

## Check PSR-4

 - On aurait du le faire avec Laravel Microscope, mais le check n'est pas complet
 - Alternative : utiliser `composer dumpautoload`, les fichiers non conforme devraient apparaitre en warning

## Régénérer les qr-codes

- Commande `php artisan custom:regenerate-qrcodes`
- Vérifier que les qr-codes sont encore lisibles par un lecteur