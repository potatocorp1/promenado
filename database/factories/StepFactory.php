<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StepFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(3),
            'code' => $this->faker->ean13(),
            'itinerary' => $this->faker->paragraph(),
            'on_site' => $this->faker->paragraph(),
        ];
    }
}
