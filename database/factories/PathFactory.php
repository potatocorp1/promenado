<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PathFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(3),
            'duration' => $this->faker->regexify('[1-5]{1}h [0-5]{1}0m'),
            'description' => $this->faker->paragraph(),
            'start_place' => $this->faker->address(),
            'is_public' => $this->faker->boolean(),
        ];
    }
}
