<?php

namespace Database\Seeders;

use App\Models\Path;
use App\Models\Step;
use Illuminate\Database\Seeder;

class StepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = Path::first();
        if ($path === null) {
            $path = Path::factory()->create();
        }

        Step::factory()
            ->create([
                'path_id' => $path->id
            ]);
    }
}
