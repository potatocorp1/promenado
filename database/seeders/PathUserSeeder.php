<?php

namespace Database\Seeders;

use App\Models\Path;
use App\Models\User;
use Illuminate\Database\Seeder;

class PathUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = Path::factory()->create();
        $user = User::factory()->hasAttached(
            Path::factory()->count(3)
        )->create();
    }
}
