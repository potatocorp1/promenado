<?php

namespace Database\Seeders;

use App\Models\Path;
use App\Models\Step;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Path::factory()->count(5)->create()->each(function ($path) {
            for ($i = 1; $i < rand(1, 5); $i++) {
                Step::factory()->create([
                    'path_id' => $path->id
                ]);
            }
        });
        User::factory()->create([
            'name' => 'Kernelle Admin',
            'email' => 'admin@admin.fr',
            'password' => Hash::make('useruser'),
            'is_admin' => true,
        ]);
        User::factory()->create([
            'name' => 'Mojito Promeneur',
            'email' => 'user@user.fr',
            'password' => Hash::make('useruser'),
            'is_admin' => false,
        ]);
    }
}
