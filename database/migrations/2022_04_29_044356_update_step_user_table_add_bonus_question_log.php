<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateStepUserTableAddBonusQuestionLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('step_user', function (Blueprint $table) {
            $table->timestamp('correctly_answered_at')->nullable();
            $table->integer('question_nb_attempts')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('step_user', function (Blueprint $table) {
            $table->dropColumn('correctly_answered_at');
            $table->dropColumn('question_nb_attempts');
        });
    }
}
