<?php

namespace App\Observers;

use App\Models\Path;
use App\Models\Step;

class StepObserver
{
    /**
     * Handle the Step "created" event.
     *
     * @param  \App\Models\Step  $step
     * @return void
     */
    public function created(Step $step)
    {
        $path = Path::find($step->path_id);
        $stepsOrder = $path->steps_order;
        if (! is_array($path->steps_order)) {
            $stepsOrder = [];
        }
        $stepsOrder[] = $step->id;
        $path->steps_order = $stepsOrder;
        $path->save();
    }

    /**
     * Handle the Step "deleted" event.
     *
     * @param  \App\Models\Step  $step
     * @return void
     */
    public function deleted(Step $step)
    {
        $path = Path::find($step->path_id);
        if (! is_array($path->steps_order)) {
            return;
        }
        $stepsOrder = array_filter($path->steps_order, static function ($element) use ($step) {
            return $element !== $step->id;
        });

        $path->steps_order = array_values($stepsOrder);
        $path->save();
    }
}
