<?php

namespace App\Services;

use App\Models\Path;
use App\Models\Step;
use Endroid\QrCode\Logo\Logo;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Writer\PngWriter;
use Illuminate\Support\Facades\Storage;

class QrcodeService
{
    public function generateForPath(Path $path)
    {
        $writer = new PngWriter();
        $qrCode = QrCode::create(route('path.show', $path->id));
        $result = $writer->write($qrCode);
        Storage::makeDirectory('/public/qrcodes/');
        $qrcodeFilename = storage_path().'/app/public/qrcodes/path_'.$path->id.'.png';
        $result->saveToFile($qrcodeFilename);
        return;
    }

    public function generateForStep(Step $step)
    {
        $writer = new PngWriter();
        if ($step->code == '') {
            return;
        }
        $qrCode = QrCode::create(route('step.complete.by.url', ['step' => $step->id, 'code' => $step->code]));
        $result = $writer->write($qrCode);
        Storage::makeDirectory('/public/qrcodes/');
        $qrcodeFilename = storage_path().'/app/public/qrcodes/step_'.$step->id.'.png';
        $result->saveToFile($qrcodeFilename);
        return;
    }
}
