<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PathUser extends Pivot
{
    protected $casts = [
        'started_at' => 'datetime:Y-m-d',
        'completed_at' => 'datetime:Y-m-d'
    ];
}
