<?php

namespace App\Models;

use App\Http\Traits\AttachableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    use HasFactory;
    use AttachableTrait;

    protected $fillable = [
        'name',
        'code',
        'itinerary',
        'on_site',
        'path_id',
        'cover',
        'hint',
        'question',
        'answer_options',
        'answer_index',
    ];

    protected $casts = [
        'answer_options' => 'array',
    ];

    public function path()
    {
        return $this->belongsTo(Path::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('completed_at', 'hint_used', 'correctly_answered_at', 'question_nb_attempts');
    }

    public function isCompletedForUser(User $user): bool
    {
        $relation = $this->users()->firstWhere('user_id', $user->id);
        if ($relation == null) {
            return false;
        }
        return ($relation->pivot->completed_at !== null);
    }

    public function hintCanBeRequestedBy(User $user): bool
    {
        if (! $this->hint) {
            return false;
        }
        return ! $this->hintAlreadyUsedBy($user);
    }

    public function hintIsDisplayedFor(User $user): bool
    {
        if (! $this->hint) {
            return false;
        }
        return $this->hintAlreadyUsedBy($user);
    }

    public function hintAlreadyUsedBy(User $user): bool
    {
        $relation = $this->users()->firstWhere('user_id', $user->id);
        if ($relation == null) {
            return false;
        }
        return $relation->pivot->hint_used;
    }

    public function questionIsAlreadyAnsweredBy(User $user): bool
    {
        $log = $this->users()->firstWhere('user_id', $user->id)->pivot;
        return $log->correctly_answered_at !== null;
    }

    public function questionNbAttempsFor(User $user): int
    {
        $log = $this->users()->firstWhere('user_id', $user->id);
        if ($log == null) {
            return 0;
        }
        return $log->pivot->question_nb_attempts;
    }
}
