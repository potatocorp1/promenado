<?php

namespace App\Models;

use App\Http\Traits\AttachableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Path extends Model
{
    use HasFactory;
    use AttachableTrait;

    protected $attributes = [
        'is_public' => false,
    ];

    protected $fillable = [
        'name',
        'duration',
        'description',
        'start_place',
        'cover',
        'is_public',
        'steps_order',
        'timeline',
    ];

    protected $casts = [
        'steps_order' => 'array',
        'timeline' => 'array',
    ];

    public function steps()
    {
        return $this->hasMany(Step::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->using(PathUser::class)->withPivot(
            'started_at',
            'completed_at',
            'timeline_completed_at',
        );
    }

    public function qrcode(): string
    {
        return public_path('storage/qrcodes/path_'.$this->id.'.png');
    }

    public function getStatusForUser(User $user): string
    {
        if (! $this->users->contains($user->id)) {
            return 'not_started';
        }

        $completion_date = $this->users()->firstWhere('user_id', $user->id)->pivot->completed_at;
        if ($completion_date !== null) {
            return 'completed';
        }

        return 'started';
    }

    public function getProgressionPercentage(User $user): int
    {
        $nbSteps = $this->steps->count();
        if ($nbSteps == 0) {
            return 0;
        }

        $nbStepCompleted = 0;
        foreach ($this->steps as $step) {
            if ($step->isCompletedForUser($user)) {
                $nbStepCompleted++;
            }
        }
        return $nbStepCompleted * 100 / $nbSteps;
    }

    public function sortSteps(): void
    {
        if ($this->steps_order == null) {
            return;
        }

        $order = $this->steps_order;
        $this->steps = $this->steps->sortBy(function ($step) use ($order) {
            return array_search($step->id, $order);
        })->values();
    }

    public function sortStepsByYears(): void
    {
        if (! $this->hasTimeline()) {
            return;
        }

        $years = array_flip($this->timeline['years']);
        $this->steps = $this->steps->sortBy(function ($step) use ($years) {
            return array_search($step->id, $years);
        })->values();
    }

    public function getNextStep(User $user): ?Step
    {
        $this->sortSteps();
        foreach ($this->steps as $step) {
            if (! $step->isCompletedForUser($user)) {
                return $step;
            }
        }
        return $this->steps->first() ?? null;
    }

    public function hasTimeline(): bool
    {
        if ($this->timeline == null) {
            return false;
        }

        foreach (array_keys($this->timeline['years']) as $step_id) {
            if (! $this->steps->contains($step_id)) {
                return false;
            }
        }
        return true;
    }

    public function timelineCompletedBy(User $user): bool
    {
        if (! $this->users->contains($user->id)) {
            return false;
        }

        $log = $this->users()->firstWhere('user_id', $user->id)->pivot;
        return $log->timeline_completed_at !== null;
    }
}
