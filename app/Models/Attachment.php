<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Attachment extends Model
{
    use HasFactory;

    public $guarded = [];

    public $appends = ['url'];

    public static function boot()
    {
        parent::boot();
        self::deleted(function ($attachement) {
            $attachement->deleteFile();
        });
    }

    public function attachable()
    {
        return $this->morphTo();
    }

    public function getUrlAttribute()
    {
        return '/storage/uploads/'.$this->name;
    }

    public function deleteFile()
    {
        Storage::disk('public')->delete('/uploads/'.$this->name);
    }
}
