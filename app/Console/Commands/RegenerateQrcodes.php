<?php

namespace App\Console\Commands;

use App\Models\Path;
use App\Models\Step;
use App\Services\QrcodeService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class RegenerateQrcodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom:regenerate-qrcodes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate all qrcodes (usefull after domain or url modification)';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(QrcodeService $service)
    {
        $allQrcodes = Storage::files('public/qrcodes/');
        Storage::delete($allQrcodes);

        $this->withProgressBar(Path::all(), function ($path) use ($service) {
            $service->generateForPath($path);
        });
        $this->withProgressBar(Step::all(), function ($step) use ($service) {
            $service->generateForStep($step);
        });

        return Command::SUCCESS;
    }
}
