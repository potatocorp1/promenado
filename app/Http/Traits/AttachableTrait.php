<?php

namespace App\Http\Traits;

use App\Models\Attachment;

trait AttachableTrait
{
    public static function bootAttachableTrait()
    {
        self::deleted(function ($subject) {
            foreach ($subject->attachments()->get() as $attachment) {
                $attachment->deleteFile();
            }
            $subject->attachments()->delete();
        });

        self::updating(function ($subject) {
            $richTextChanged = (
                ($subject->itinerary != $subject->getOriginal('itinerary'))
                || ($subject->hint != $subject->getOriginal('hint'))
                || ($subject->on_site != $subject->getOriginal('on_site'))
                || ($subject->on_site != $subject->getOriginal('description'))
            );
            if ($richTextChanged) {
                $images = array_merge(
                    self::getImagesForAttribute($subject, 'itinerary'),
                    self::getImagesForAttribute($subject, 'hint'),
                    self::getImagesForAttribute($subject, 'on_site'),
                    self::getImagesForAttribute($subject, 'description'),
                );
                $attachments = $subject->attachments()->whereNotIn('name', $images);
                foreach ($attachments->get() as $attachment) {
                    $attachment->deleteFile();
                }
                $attachments->delete();
            }
        });
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    private static function getImagesForAttribute($subject, string $attribute): array
    {
        $images = [];
        if (preg_match_all('/src="([^"]+)"/', $subject->{$attribute}, $matches) > 0) {
            $images = array_map(function ($match) {
                return basename($match);
            }, $matches[1]);
        }
        return $images;
    }
}
