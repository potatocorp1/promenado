<?php

namespace App\Http\Controllers;

use App\Models\Attachment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AttachmentController extends Controller
{
    private $rules = [
        'attachable_type' => 'required',
        'attachable_id' => 'required|int',
        'image' => 'required|image',
    ];

    /**
     * Store a new attachment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->rules);
        $type = $request->get('attachable_type');
        $id = $request->get('attachable_id');
        $file = $request->file('image');

        if (! (class_exists($type) && method_exists($type, 'attachments'))) {
            return new JsonResponse(['attachable_type' => 'Ce contenu ne peut pas recevoir de fichiers attachés'], 422);
        }

        $subject = call_user_func($type . '::find', $id);
        if (! $subject) {
            return new JsonResponse(['attachable_id' => 'Ce contenu ne peut pas recevoir de fichiers attachés'], 422);
        }

        $classParts = explode('\\', $type);
        $className = strtolower(end($classParts));
        $fileName = $className.'_'.$id.'_'.basename($file).'.'.$file->getClientOriginalExtension();
        $attachment = new Attachment([
            'attachable_type' => $type,
            'attachable_id' => $id,
            'name' => $fileName,
        ]);
        $attachment->save();
        $file->storeAs('public/uploads', $fileName);

        return new JsonResponse($attachment);
    }
}
