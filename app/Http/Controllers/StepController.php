<?php

namespace App\Http\Controllers;

use App\Models\Step;
use App\Models\Path;
use App\Services\QrcodeService;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Laravel\Facades\Image;

class StepController extends Controller
{
    private $rules = [
        'path_id' => 'required|exists:paths,id',
        'name' => 'required|max:255',
        'code' => 'required',
        'itinerary' => 'required',
        'on_site' => 'required',
        'question' => 'max:255',
        'answer_options' => 'required_unless:question,null',
        'answer_options.*' => 'required_unless:question,null',
        'answer_index' => 'integer|min:0|max:2',
    ];

    private $defaultEmptyOptions = ['', '', ''];

    /**
     * Show the form to create a new step.
     *
     * @return \Illuminate\View\View
     */
    public function create($pathId)
    {
        $step = new Step([
            'path_id' => $pathId,
            'name' => '',
            'code' => '',
            'itinerary' => '',
            'on_site' => '',
        ]);
        $step->save();
        $step->answer_options = $this->defaultEmptyOptions;
        return view('step.form', [
            'title' => 'Nouvelle étape',
            'buttonLabel' => "Créer l'étape",
            'method' => 'PUT',
            'action' => route('step.update', $step->id),
            'step' => $step
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Step $step
     * @return \Illuminate\Http\Response
     */
    public function destroy(Step $step)
    {
        $step->delete();
        return redirect()->route('path.show.admin', $step->path_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Step $step
     * @return \Illuminate\Http\Response
     */
    public function edit(Step $step)
    {
        $step->answer_options = $step->answer_options ?? $this->defaultEmptyOptions;
        return view('step.form', [
            'title' => "Modifier l'étape",
            'buttonLabel' => 'Enregistrer les modifications',
            'method' => 'PUT',
            'action' => route('step.update', $step->id),
            'step' => $step
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Step $step
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Step $step, QrcodeService $service)
    {
        $request->validate($this->rules);

        if ($request->file('cover') !== null) {
            $file = $request->file('cover');
            $fileName = 'step_'.$step->id.'.'.$file->getClientOriginalExtension();
            $filePath = $file->storeAs('public/covers', $fileName);

            $imagePath = storage_path('app/'.$filePath);
            Image::read($imagePath)->scale(1024)->save($imagePath);

            if ($step->cover !== null) {
                Storage::delete($step->cover);
            }
            $step->update(array_merge($request->all(), ['cover' => 'covers/'.$fileName]));
        } else {
            $step->update($request->all());
        }

        if ($step->question === null || trim($step->question) === '') {
            $step->answer_options = null;
        }

        $step->save();
        $service->generateForStep($step);

        return redirect()->route('path.show.admin', $step->path_id)->with('status_success', 'Parcours mis à jour !');
    }

    /**
     * Display step page.
     *
     * @param  Step $step
     * @return \Illuminate\Http\Response
     */
    public function show(Step $step)
    {
        $path = Path::findOrFail($step->path_id);
        $user = Auth::user();

        if ($step->isCompletedForUser(Auth::user())) {
            return view('step.complete', [
                'step' => $step,
                'progress' => $path->getProgressionPercentage($user),
            ]);
        }

        return view('step.ongoing', [
            'step' => $step,
            'progress' => $path->getProgressionPercentage($user),
        ]);
    }

    /**
     * Display hint for a step.
     *
     * @param  Step $step
     * @return \Illuminate\Http\Response
     */
    public function askForHint(Step $step)
    {
        $user = Auth::user();
        if ($step->hintCanBeRequestedBy($user)) {
            $step->users()->attach($user->id, ['hint_used' => true]);
        }

        return redirect()->route('step.show', [
            'step' => $step
        ]);
    }

    /**
     * Complete a step with its secret code.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Step $step
     * @param  string $code
     * @return \Illuminate\Http\Response
     */
    public function complete(Request $request, Step $step, string $code = null)
    {
        $user = Auth::user();
        $codeToCheck = $code ?? $request->input('code');

        /* Fix for badly printed panel with wrong code */
        if (in_array($step->id, [36, 78]) && $codeToCheck == "Anguillettes") {
            $codeToCheck = "Rue";
        }

        $codeIsNotNull = $codeToCheck !== null;
        $codeIsCorrect = $codeIsNotNull && ($this->sanitizeCode($step->code) == $this->sanitizeCode($codeToCheck));

        /** Fix for short path with same physical panels than the long one */
        $commonSteps = [
            26 => 69,
            27 => 68,
            29 => 67,
            36 => 78,
            46 => 53,
            47 => 52,
            48 => 51,
        ];
        $shouldRedirect = $this->shouldRedirectToShortPathForSaintSauvant($step, $commonSteps);
        if ($shouldRedirect) {
            return redirect()->route('step.complete.by.url', [
                'step' => $commonSteps[$step->id],
                'code' => $code
            ]);
        }

        /** Nominal case */
        if (! $codeIsCorrect) {
            return redirect()->route('step.show', [
                'step' => $step
            ])->with('status_warning', 'Code incorrect !');
        }

        if (! $step->isCompletedForUser($user)) {
            $relation = $step->users()->firstWhere('user_id', $user->id);
            if ($relation == null) {
                $step->users()->attach($user->id, ['completed_at' => now()]);
            } else {
                $step->users()->updateExistingPivot($user->id, ['completed_at' => now()]);
            }
        }

        return redirect()->route('step.show', [
            'step' => $step
        ]);
    }

    private function sanitizeCode(string $code): string
    {
        return strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $code));
    }

    private function shouldRedirectToShortPathForSaintSauvant(Step $step, array $commonSteps): bool
    {
        $user = Auth::user();

        if (!isset($commonSteps[$step->id])) {
            return false;
        }
        // step is ambigous

        $stepShortId = $commonSteps[$step->id];
        $stepShort = Step::find($stepShortId);
        if ($stepShort == null) {
            return false;
        }
        $pathShort = Path::find($stepShort->path_id);
        if ($pathShort == null) {
            return false;
        }
        if ($pathShort->getStatusForUser($user) != 'started') {
            return false;
        }
        // short path is started

        $pathLong = Path::find($step->path_id);
        if ($pathLong == null) {
            return true;
        }
        if ($pathLong->getStatusForUser($user) != 'started') {
            return true;
        }
        // long path is also started

        $nextStepShort = $pathShort->getNextStep($user);
        $nextStepLong = $pathLong->getNextStep($user);
        if ($nextStepShort->id == $stepShortId) {
            return true;
        }
        if ($nextStepLong->id == $stepShortId) {
            return false;
        }
        // both paths are started and it's not their next step

        return false;
    }

    /**
     * Answer the optional bonus question.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Step $step
     * @return \Illuminate\Http\Response
     */
    public function answerBonusQuestion(Request $request, Step $step)
    {
        $user = Auth::user();

        $log = $step->users()->firstWhere('user_id', $user->id)->pivot;
        $step->users()->updateExistingPivot($user->id, [
            'question_nb_attempts' => $log->question_nb_attempts + 1
        ]);

        if ($request->input('answer_index') != $step->answer_index) {
            return redirect()->route('step.show', [
                'step' => $step
            ])->with('status_warning', 'Réponse incorrecte !');
        }

        $step->users()->updateExistingPivot($user->id, [
            'correctly_answered_at' => now()
        ]);

        return redirect()->route('step.show', [
            'step' => $step
        ]);
    }

    /**
     * Display PDF to print with qrcode and other information.
     *
     * @param  Step $step
     * @return \Illuminate\Http\Response
     */
    public function download(Step $step)
    {
        $pdf = Pdf::loadView('step.download', [
            'step' => $step,
            'path' => Path::findOrFail($step->path_id),
            'filename' => 'storage/qrcodes/step_'.$step->id.'.png',
        ]);
        return $pdf->download('qrcode_step_'.$step->id.'.pdf');
    }

    /**
     * Display itinerary preview for admin.
     *
     * @param  Step $step
     * @return \Illuminate\Http\Response
     */
    public function previewItinerary(Step $step)
    {
        return view('step.preview-itinerary', [
            'step' => $step
        ]);
    }

    /**
     * Display on site preview for admin.
     *
     * @param  Step $step
     * @return \Illuminate\Http\Response
     */
    public function previewSite(Step $step)
    {
        return view('step.preview-site', [
            'step' => $step
        ]);
    }
}
