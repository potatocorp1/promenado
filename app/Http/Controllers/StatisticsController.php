<?php

namespace App\Http\Controllers;

use App\Models\Path;
use App\Models\User;
use Carbon\Carbon;

class StatisticsController extends Controller
{
    public function index()
    {
        return view('statistics.index', [
            'nbUsers' => User::all()->count(),
            'nbUsersPerYearsAndMonths' => $this->getNewUsersPerYears(),
            'statPerPath' => $this->getStatPerPath()
        ]);
    }

    private function getNewUsersPerYears(): array
    {
        $years = User::all()->groupBy(function ($user) {
            return $user->created_at->year;
        });

        $years->transform(function ($users) {
            return $this->getNewUsersPerMonths($users);
        });

        return $years->toArray();
    }

    private function getNewUsersPerMonths($users): array
    {
        $userPerMonth = $users->groupBy(function ($user) {
            return $user->created_at->month;
        })->toarray();
        $stats = [];
        for ($i = 1; $i <= 12; $i++) {
            $stats[$i] = isset($userPerMonth[$i]) ? count($userPerMonth[$i]) : 0;
        }
        return $stats;
    }

    private function getStatPerPath(): array
    {
        $statPerPath = [];
        $paths = Path::orderBy('name')->get();

        foreach ($paths as $path) {
            $nbHints = 0;
            foreach ($path->steps as $step) {
                $nbHints += $step->users()->wherePivot('hint_used', '=', true)->count();
            }

            $time = [];
            $averageTimeFormatted = '';
            foreach ($path->users as $user) {
                if ($user->pivot->completed_at !== null) {
                    $completedAt = Carbon::parse($user->pivot->completed_at);
                    $startedAt = Carbon::parse($user->pivot->started_at);
                    $time[] = $completedAt->diffInMinutes($startedAt);
                }
            }
            if (count($time)) {
                $averageTime = array_sum($time) / count($time);
                $averageTimeFormatted = (int)($averageTime / 60) . ' heures ' . ($averageTime % 60) . ' minutes';
            }

            $statPerPath[$path->id] = [
                'id' => $path->id,
                'name' => $path->name,
                'started' => $path->users()->wherePivot('started_at', '!=', 'null')->count(),
                'ended' => $path->users()->wherePivot('completed_at', '!=', 'null')->count(),
                'nbHints' => $nbHints,
                'averageTime' => $averageTimeFormatted,
            ];
        }
        return $statPerPath;
    }

    public function show($id)
    {
        $path = Path::findOrFail($id);

        $nbHintsPerUser = [];
        $timePerUser = [];
        foreach ($path->users as $user) {
            $nbHintsPerUser[$user->id] = 0;
            foreach ($path->steps as $step) {
                if ($step->hintAlreadyUsedBy($user)) {
                    $nbHintsPerUser[$user->id] += 1;
                }
            }

            $timePerUser[$user->id] = '';
            if ($user->pivot->completed_at) {
                $completedAt = Carbon::parse($user->pivot->completed_at);
                $startedAt = Carbon::parse($user->pivot->started_at);
                $time = $completedAt->diffInMinutes($startedAt);
                $timePerUser[$user->id] = (int)($time / 60) . ' heures ' . ($time % 60) . ' minutes';
            }
        }

        return view('statistics.path', [
            'path' => $path,
            'nbHints' => $nbHintsPerUser,
            'time' => $timePerUser,
        ]);
    }
}
