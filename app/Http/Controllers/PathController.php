<?php

namespace App\Http\Controllers;

use App\Models\Path;
use App\Services\QrcodeService;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Laravel\Facades\Image;

class PathController extends Controller
{
    private $rules = [
        'name' => 'required|unique:paths|max:255',
        'duration' => 'required|max:255',
        'description' => 'required',
        'start_place' => 'required|max:255',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paths = Path::all();
        $ctaLink = '#';
        $firstPublicPath = Path::where(['is_public' => true])->first();
        if ($firstPublicPath) {
            $ctaLink = route('path.show', $firstPublicPath);
        }
        return view('welcome', [
            'paths' => $paths,
            'ctaLink' => $ctaLink
        ]);
    }

    /**
     * Show the public details for a given path.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function showAnonymous($id)
    {
        return view('path.showAnonymous', [
            'path' => Path::findOrFail($id)
        ]);
    }

    /**
     * Show steps list for a given path.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function showUser($id)
    {
        $path = Path::findOrFail($id);
        $user = Auth::user();

        if ($path->getStatusForUser($user) == 'completed') {
            if ($path->hasTimeline() && !$path->timelineCompletedBy($user)) {
                $timelineSteps = $path->steps->only(array_keys($path->timeline['years']));
                return view('path.timeline', [
                    'path' => $path,
                    'timelineSteps' => $timelineSteps
                ]);
            }

            $results = [];
            $score = 0;
            foreach ($path->steps as $step) {
                $results[$step->id] = [
                    'name' => $step->name,
                    'base' => '+5',
                    'hint' => '',
                    'bonus' => ''
                ];
                $score += 5;
                if ($step->hintAlreadyUsedBy($user)) {
                    $results[$step->id]['hint'] = '-1';
                    $score -= 1;
                }
                if ($step->question) {
                    $nbAttempts = $step->questionNbAttempsFor($user);
                    $bonusPoints = match ($nbAttempts) {
                        1 => 2,
                        2 => 1,
                        default => 0
                    };
                    $results[$step->id]['bonus'] = '+' . $bonusPoints;
                    $score += $bonusPoints;
                }
            }
            return view('path.end', [
                'name' => $path->name,
                'pathId' => $path->id,
                'results' => $results,
                'score' => $score,
            ]);
        }

        $path->sortSteps();
        return view('path.ongoing', [
            'path' => $path,
            'progress' => $path->getProgressionPercentage($user),
        ]);
    }

    /**
     * Show the details for a given path.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function showAdmin($id, QrcodeService $service)
    {
        $path = Path::findOrFail($id);
        $path->sortSteps();

        if (! file_exists($path->qrcode())) {
            $service->generateForPath($path);
        }
        return view('path.showAdmin', [
            'path' => $path
        ]);
    }

    /**
     * Show the form to create a new path.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $path = new Path([
            'name' => '',
            'duration' => '',
            'description' => '',
            'start_place' => '',
        ]);
        $path->save();
        return view('path.form', [
            'title' => 'Nouveau parcours',
            'buttonLabel' => 'Créer le parcours',
            'method' => 'PUT',
            'action' => route('path.update', $path->id),
            'path' => $path
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Path $path
     * @return \Illuminate\Http\Response
     */
    public function edit(Path $path)
    {
        return view('path.form', [
            'title' => 'Modifier le parcours',
            'buttonLabel' => 'Enregistrer les modifications',
            'method' => 'PUT',
            'action' => route('path.update', $path->id),
            'path' => $path
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Path $path
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Path $path)
    {
        $this->rules['name'] = [
            'required',
            Rule::unique('paths')->ignore($path->id),
            'max:255'
        ];
        $request->validate($this->rules);


        if ($request->file('cover') !== null) {
            $file = $request->file('cover');
            $fileName = 'path_'.$path->id.'.'.$file->getClientOriginalExtension();
            $filePath = $file->storeAs('public/covers', $fileName);

            $imagePath = storage_path('app/'.$filePath);
            Image::read($imagePath)->scale(1024)->save($imagePath);

            if ($path->cover !== null) {
                Storage::delete($path->cover);
            }
            $path->update(array_merge($request->all(), ['cover' => 'covers/'.$fileName]));
        } else {
            $path->update($request->all());
        }

        $path->save();

        return redirect()->route('path.show.admin', $path->id)->with('status_success', 'Parcours mis à jour !');
    }

    public function editTimeline(Path $path)
    {
        return view('path.formTimeline', [
            'path' => $path
        ]);
    }

    public function updateTimeline(Request $request, Path $path)
    {
        $rules = [
            'year' => 'required',
            'year.*' => 'nullable|integer|min:1000|max:2500',
        ];
        $request->validate($rules);

        $timeline = array_filter($request->get('year'), static function ($element) {
            return !empty($element);
        });

        $path->update([
            'timeline' => [
                'years' => $timeline
            ]
        ]);
        $path->save();
        return redirect()->route('path.show.admin', $path->id)->with('status_success', 'Timeline mise à jour !');
    }

    public function previewTimeline(Path $path)
    {
        $path->sortStepsByYears();
        return view('path.previewTimeline', [
            'path' => $path
        ]);
    }

    /**
     * Update the path's steps order.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Path $path
     * @return \Illuminate\Http\Response
     */
    public function updateStepsOrder(Request $request, Path $path)
    {
        $request->validate(['steps_order' => 'array|required', 'steps_order:*' => 'integer']);

        $path->steps_order = $request->get('steps_order');
        $path->save();

        return response()->json($path, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Path $path
     * @return \Illuminate\Http\Response
     */
    public function destroy(Path $path)
    {
        foreach ($path->steps as $step) {
            $step->delete();
        }
        $path->delete();
        return redirect()->route('main');
    }

    /**
     * Add log when a user starts a path.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function start(Path $path)
    {
        $user = Auth::user();
        $pathAlreadyStarted = $path->users->contains($user->id);

        if (!$pathAlreadyStarted) {
            $path->users()->attach($user->id, ['started_at' => now()]);
        }

        return redirect()->route('step.show', $path->getNextStep($user));
    }

    /**
     * Delete previous progression and redirect to public view.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function restart(Path $path)
    {
        $user = Auth::user();

        foreach ($path->steps as $step) {
            if ($step->users->contains($user->id)) {
                $step->users()->detach($user->id);
            }
        }

        if ($path->users->contains($user->id)) {
            $path->users()->detach($user->id);
        }

        return redirect()->route('path.show', $path->id);
    }

    /**
     * Add log and display ending page for a path.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function end(Path $path)
    {
        $user = Auth::user();

        $pathProgressionPercentage = $path->getProgressionPercentage($user);
        if ($pathProgressionPercentage !== 100) {
            return redirect()->route('path.start', [
                'path' => $path
            ]);
        }

        $completion_date = $path->users()->firstWhere('user_id', $user->id)->pivot->completed_at;
        if ($completion_date == null) {
            $path->users()->updateExistingPivot($user->id, [
                'completed_at' => now(),
            ]);
        }

        return redirect()->route('path.show.user', $path);
    }

    /**
     * Activate or desactivate a path for public view.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function togglePublicStatus(Path $path)
    {
        $path->is_public = !$path->is_public;
        $path->save();
        return redirect()->route('path.show.admin', $path->id)
            ->with('status_success', $path->is_public ? 'Le parcours est maintenant visible par les utilisateurs' : "Le parcours n'est plus visible par les utilisateurs");
    }

    /**
     * Display PDF to print with qrcode and other information.
     *
     * @param  Path $path
     * @return \Illuminate\Http\Response
     */
    public function download(Path $path)
    {
        $pdf = Pdf::loadView('path.download', [
            'path' => $path
        ]);
        return $pdf->download($path->qrcode());
    }

    /**
     * Complete the timeline for a path
     *
     * @param  Path $path
     * @return \Illuminate\Http\Response
     */
    public function completeTimeline(Path $path)
    {
        $user = Auth::user();

        $path->users()->updateExistingPivot($user->id, [
            'timeline_completed_at' => now()
        ]);

        return response()->noContent();
    }
}
