<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class UserController extends Controller
{
    public function updateName(Request $request)
    {
        /** @var mixed $user */
        $user = Auth::user();
        $request->validate(['name' => ['required', 'string', 'max:255']]);

        $user->update(['name' => $request->name]);
        $user->save();

        return redirect()->route('dashboard');
    }

    public function updatePassword(Request $request)
    {
        /** @var mixed $user */
        $user = Auth::user();
        $attributes = [
            'password' => 'mot de passe'
        ];
        $rules = [
            'oldPassword' => ['required', 'string', 'max:255'],
            'password' => ['required', 'confirmed', Password::defaults()]
        ];
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        $validator->validate();

        $checkPassword = Auth::attempt([
            'email' => $user->email,
            'password' => $request->oldPassword
        ]);
        if (! $checkPassword) {
            return redirect()->route('user.edit-password')->with('status_warning', 'Mot de passe actuel incorrect');
        }

        $user->update(['password' => Hash::make($request->password)]);
        $user->save();

        return redirect()->route('dashboard');
    }

    public function destroy(User $user)
    {
        /** @var mixed $authUser */
        $authUser = Auth::user();
        if ($authUser->id !== $user->id) {
            return response()->view('error.unauthorized', [], 403);
        }

        $user->delete();
        return redirect()->route('main');
    }
}
