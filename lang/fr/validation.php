<?php

return [

    'confirmed' => 'Le :attribute et sa confirmation ne correspondent pas.',
    'min' => [
        'string' => 'Le :attribute doit contenir au moins :min caractères.',
    ],
    'unique' => 'Le :attribute est déjà utilisé.',
    'required_unless' => 'Le champ :attribute est obligatoire à moins que le champ :other soit :values.',

];
